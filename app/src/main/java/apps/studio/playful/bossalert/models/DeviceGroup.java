package apps.studio.playful.bossalert.models;

import android.support.v4.util.ArrayMap;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import static apps.studio.playful.bossalert.helpers.Constants.NOTIFICATION_KEY;
import static apps.studio.playful.bossalert.helpers.Constants.NOTIFICATION_KEY_NAME;
import static apps.studio.playful.bossalert.helpers.Constants.TOKENS;

public class DeviceGroup {

    public String notificationKeyName;
    public String notificationKey;
    public ArrayMap<String, String> tokens = new ArrayMap<>();

    static public DeviceGroup fromJson(JSONObject json) throws JSONException
    {
        DeviceGroup deviceGroup = new DeviceGroup();

        // Read {"notificationKeyName": "name", "notificationKey": "123456789"}
        deviceGroup.notificationKeyName = json.getString(NOTIFICATION_KEY_NAME);
        deviceGroup.notificationKey = json.optString(NOTIFICATION_KEY);

        // Read "tokens": {"token1" : "1234", "token2" : "3434"}
        JSONObject jsonTokens = json.optJSONObject(TOKENS);
        if (jsonTokens != null) {
            Iterator<String> jsonTokensIterator = jsonTokens.keys();
            while (jsonTokensIterator.hasNext()) {
                String tokenName = jsonTokensIterator.next();
                deviceGroup.tokens.put(tokenName, jsonTokens.getString(tokenName));
            }
        }
        return deviceGroup;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(NOTIFICATION_KEY_NAME, notificationKeyName);
        json.put(NOTIFICATION_KEY, notificationKey);
        json.put(TOKENS, new JSONObject(tokens));
        return json;
    }

}
