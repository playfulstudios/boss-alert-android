package apps.studio.playful.bossalert.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Group is a class to handle the meta data of the groups stored / created for FireBase
 *
 */
@IgnoreExtraProperties
public class Group {

    public String nickName;
    public String password;
    public String salt; // Only used for creating topics
    public boolean locked;
    public String dateCreated;
    public String owner;
    public LastMessage lastMessage;

    /**
     * FireBase requires public access of class variables
     */
    private String name;

    /**
     * Blank constructor for FireBase
     */
    public Group() {
    }

    /**
     * Alternate constructor #1
     * Used to create a non existing group (in FireBase)
     *
     * @param owner    The owner of the new group
     * @param password The password (hashed)
     */
    public Group(String name, String owner, String password) {
        this.name = name;
        this.nickName = name;
        this.owner = owner;
        this.password = password;
        this.salt = Password.generateSalt();
        this.locked = false;
        this.dateCreated = setDate();
        this.lastMessage = null;
    }

    /**
     * Alternate constructor #2
     * Used to fetch an existing group from FireBase
     *
     * @param name        Name of the group
     * @param password    Hashed Password
     * @param salt        Salt
     * @param dateCreated The date the group was created
     * @param owner       Owner of group
     * @param members     Members in the group
     */
    public Group(String name, String nickName, String password, String salt, boolean locked, String dateCreated, String owner, String[] members, LastMessage lastMessage) {
        this.name = name;
        this.nickName = nickName;
        this.password = password;
        this.salt = salt;
        this.locked = locked;
        this.dateCreated = dateCreated;
        this.owner = owner;
        this.lastMessage = lastMessage;
    }

    /**
     * Returns the current date in which this group was created
     *
     * @return Current date
     */
    private String setDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /* Getters and Setter **/

    @Exclude
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public LastMessage getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(LastMessage lastMessage) {
        this.lastMessage = lastMessage;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        return name.equals(group.name);

    }

    @Override
    public int hashCode() {
        int result = dateCreated.hashCode();
        result = 31 * result + owner.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}