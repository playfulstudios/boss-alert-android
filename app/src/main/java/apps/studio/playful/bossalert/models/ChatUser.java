package apps.studio.playful.bossalert.models;

import android.graphics.Bitmap;

import com.google.firebase.database.IgnoreExtraProperties;


@IgnoreExtraProperties
public class ChatUser {

    private String id;
    private String profilePicture;
    private Bitmap profilePictureBitmap;
    private String displayName;

    // Empty constructor for Firebase
    public ChatUser() {

    }

    public ChatUser(String id) {
        this.id = id;
        this.profilePicture = null;
        this.profilePictureBitmap = null;
        this.displayName = "user";
    }

    public ChatUser(String id, String profilePicture, Bitmap profilePictureBitmap, String displayName) {
        this.id = id;
        this.profilePicture = profilePicture;
        this.profilePictureBitmap = profilePictureBitmap;
        this.displayName = displayName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public Bitmap getProfilePictureBitmap() {
        return profilePictureBitmap;
    }

    public void setProfilePictureBitmap(Bitmap profilePictureBitmap) {
        this.profilePictureBitmap = profilePictureBitmap;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return "ChatUser{" +
                "id='" + id + '\'' +
                ", profilePicture='" + profilePicture + '\'' +
                ", profilePictureBitmap=" + profilePictureBitmap +
                ", displayName='" + displayName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ChatUser)) {
            return false;
        }

        ChatUser other = (ChatUser) obj;
        return other.getId().equals(getId());

    }
}
