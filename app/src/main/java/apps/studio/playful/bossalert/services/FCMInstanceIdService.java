package apps.studio.playful.bossalert.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import apps.studio.playful.bossalert.helpers.LogUtils;
import apps.studio.playful.bossalert.models.Sender;

public class FCMInstanceIdService  extends FirebaseInstanceIdService {

    public static final String TAG = "FCMIIdService";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String refreshedToken) {
        Sender entry = Sender.getInstance(getApplication().getApplicationContext());
        if (entry == null) {
            LogUtils.Error(TAG, "Could not save token, missing sender ID");
        } else {
            entry.setAppToken(refreshedToken);
            entry.save();
        }
    }
}
