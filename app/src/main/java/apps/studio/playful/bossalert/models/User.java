package apps.studio.playful.bossalert.models;


import java.util.ArrayList;
import java.util.List;


public class User {

    private String id;
    private String displayName;
    private String profilePicture;
    private String email;
    private List<String> groups;
    private String favouriteGroup;

    // Empty constructor required by FireBase
    public User() {

    }

    public User(String id, String displayName, String profilePicture, String email, List<String> groups,
                String favouriteGroup) {
        setId(id);
        setDisplayName(displayName);
        setProfilePicture(profilePicture);
        setEmail(email);
        setGroups(groups);
        setFavouriteGroup(favouriteGroup);
    }

    /**
     * Adds a new group to the users list
     * TODO: Add max group count
     *
     * @param group The group to add
     */
    public void addGroup(String group) {
        if (this.groups == null) {
            // Groups doesn't exist so create a new array list to handle group
            this.groups = new ArrayList<>();
        }

        // Add the new group to the groups
        if (!this.groups.contains(group)) {
            // Prevent duplicate entries
            this.groups.add(group);
        }
    }


    /* Getters and Setter */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getEmail() {
        if (email == null)
        {
            return "";
        }
        else
        {
            return email;
        }
    }

    public void setEmail(String email) {
        this.email = email == null ? "" : email;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void initGroups()
    {
        if (this.groups == null)
        {
            this.setGroups(new ArrayList<String>());
        }
    }

    public void setGroups(List<String> groups) {

        if (groups == null)
        {
            initGroups();
        } else {
            this.groups = groups;
        }
    }

    public String getFavouriteGroup() {
        return favouriteGroup;
    }

    public void removeGroup(String group) {
        groups.remove(group);
    }


    /**
     * Sets the users favourite group and swaps out the old favourite for the new position
     *
     * @param favouriteGroup new favourite group name
     */
    public void setFavouriteGroup(String favouriteGroup) {

        if (groups != null)
        {
            if (groups.size() >= 2) {
                int oldIndex = groups.indexOf(favouriteGroup);
                if (oldIndex != -1) {
                    String tmp = groups.get(0);
                    groups.set(0, groups.get(oldIndex));
                    groups.set(oldIndex, tmp);
                }
            }
        }

        this.favouriteGroup = favouriteGroup;
    }
}
