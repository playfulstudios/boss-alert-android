package apps.studio.playful.bossalert.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.ui.MainActivity;

/**
 * Home page should have a button to enable sending alerts the the primary group as well as an option
 * to add additional groups to the subscribed topics
 */
@Deprecated
public class HomeFragment extends Fragment {

    public static final String TAG = "HomeFragmnet";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity) getActivity()).showFloatingActionButton();
        ((MainActivity) getActivity()).setToolbarTitle(getString(R.string.app_name));
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    //TODO: Add GUI features for the home page
}
