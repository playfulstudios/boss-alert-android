package apps.studio.playful.bossalert.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.helpers.Constants;
import apps.studio.playful.bossalert.logic.NotificationSender;

public class GroupMessageSendDialog extends AlertDialog {

    private LayoutInflater mInflater;
    private String groupName;
    private int groupId;
    private NotificationSender mNotificationSender;
    private Context mContext;

    private EditText messageEditText;

    public GroupMessageSendDialog(Context context, String groupName, NotificationSender notificationSender, LayoutInflater inflater) {
        super(context);

        this.groupName = groupName;

        mNotificationSender = notificationSender;
        mContext = context;
        mInflater = inflater;

        // Load the view
        View messageView = mInflater.inflate(R.layout.dialog_message, null);

        // Show the keyboard
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        // Set the elements of the dialog
        setTitle("Send a message to " + this.groupName + ": ");
        setView(messageView);
        messageEditText = messageView.findViewById(R.id.et_dialog_message);


        // Set false button
        setButton(AlertDialog.BUTTON_POSITIVE, "Send", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Never called
            }
        });

        setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = messageEditText.getText().toString();
                if (message.isEmpty()) {
                    messageEditText.setError("Can't send an empty message");
                } else {
                    // Send alert message
                    mNotificationSender.sendMessageToTopic(
                            groupName,
                            message,
                            Constants.MESSAGE_SOURCE_ALERT
                    );
                    closeDialog();
                }
            }
        });

        getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDialog();
            }
        });
    }

    private void closeDialog() {
        // Hide the keyboard once message has been sent
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(messageEditText.getWindowToken(), 0);
        dismiss();
    }
}
