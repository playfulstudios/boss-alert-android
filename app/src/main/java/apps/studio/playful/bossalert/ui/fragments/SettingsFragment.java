package apps.studio.playful.bossalert.ui.fragments;


import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.ui.MainActivity;

import static apps.studio.playful.bossalert.helpers.Constants.ORANGE;

/**
 * Handles settings and preferences
 */
public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String TAG = "SettingsFragment";
    private static String oldTheme;

    SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((MainActivity) getActivity()).setToolbarTitle("Settings");
        ((MainActivity) getActivity()).hideFloatingActionButton();

        Preference versionPref = findPreference("version");
        try {
            versionPref.setSummary(appVersion());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return super.onCreateView(inflater, container, savedInstanceState);

    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preferences); // Load xml layout

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        oldTheme = sharedPreferences.getString(getString(R.string.key_color_scheme), ORANGE);

        // Register change preference
        onSharedPreferenceChanged(sharedPreferences, getString(R.string.key_show_name));
        onSharedPreferenceChanged(sharedPreferences, getString(R.string.key_mute_alerts));
        onSharedPreferenceChanged(sharedPreferences, getString(R.string.key_system_notification));
        onSharedPreferenceChanged(sharedPreferences, getString(R.string.key_default_message));
        onSharedPreferenceChanged(sharedPreferences, getString(R.string.key_vibrate));
        onSharedPreferenceChanged(sharedPreferences, getString(R.string.key_color_scheme));
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference preference = findPreference(key);

        SharedPreferences.Editor editor = sharedPreferences.edit();


        // Handle checkbox preferences
        if (preference instanceof CheckBoxPreference)
        {
            CheckBoxPreference checkBoxPreference = (CheckBoxPreference) preference;
            // Save the current state of the checkbox to preferences
            editor.putBoolean(key, checkBoxPreference.isChecked());
            editor.apply();
        } else if (preference instanceof EditTextPreference) {
            EditTextPreference editTextPreference = (EditTextPreference) preference;
            editor.putString(key, editTextPreference.getText());
            editor.apply();
        } else if (preference instanceof SwitchPreference) {
            SwitchPreference switchPreference = (SwitchPreference) preference;
            editor.putBoolean(key, switchPreference.isChecked());
            editor.apply();
        } else if (preference instanceof ListPreference) {
            ListPreference listPreference = (ListPreference) preference;

            if (listPreference.getKey().equals(getString(R.string.key_color_scheme)))
            {

                String currentTheme = oldTheme;

                String newTheme;
                // Check if preference has default value
                if (listPreference.getEntry() == null)
                {
                    listPreference.setValue(currentTheme);
                    newTheme = currentTheme;
                }
                else
                {
                    newTheme = listPreference.getValue();
                }

                editor.putString(getString(R.string.key_color_scheme), newTheme);
                editor.commit();

                if (needsThemeChange(currentTheme, newTheme))
                {
                    oldTheme = currentTheme;
                    getActivity().recreate();
                }

            } else {

                editor.putString(key, listPreference.getValue());
                editor.apply();
            }
        }
    }

    private boolean needsThemeChange(String current, String newTheme)
    {
        boolean needsChange = false;

        if (!current.equals(newTheme))
        {
            needsChange = true;
        }

        return needsChange;
    }

    public String appVersion() throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        String version = pInfo.versionName;
        return version;
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }


}
