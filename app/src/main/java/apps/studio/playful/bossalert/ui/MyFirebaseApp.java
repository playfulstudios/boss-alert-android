package apps.studio.playful.bossalert.ui;

import com.google.firebase.database.FirebaseDatabase;

public class MyFirebaseApp extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();

        /* Enable disk persistance */
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
