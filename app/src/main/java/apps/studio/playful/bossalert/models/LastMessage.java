package apps.studio.playful.bossalert.models;

import com.google.firebase.database.ServerValue;

import java.util.HashMap;

public class LastMessage {

    private String lastMessage;
    private String userPicture;
    private String userId;
    public HashMap<String, Object> timeStamp;

    public LastMessage()
    {}

    public LastMessage(String lastMessage, String userPicture, String userId) {
        this.lastMessage = lastMessage;
        this.userPicture = userPicture;
        this.userId = userId;
        HashMap<String, Object> timeStampNow = new HashMap<>();
        timeStampNow.put("timestamp", ServerValue.TIMESTAMP);
        this.timeStamp = timeStampNow;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public HashMap<String, Object> getTimeStamp() { return this.timeStamp; }
}
