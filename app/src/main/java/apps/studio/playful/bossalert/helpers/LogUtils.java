package apps.studio.playful.bossalert.helpers;

import android.os.Build;
import android.util.Log;

import apps.studio.playful.bossalert.BuildConfig;

public class LogUtils {

    public static void Debug(final String tag, String message)
    {
        if (BuildConfig.DEBUG)
        {
            Log.d(tag, message);
        }
    }

    public static void Error(final String tag, String message)
    {
        if (BuildConfig.DEBUG)
        {
            Log.e(tag, message);
        }
    }

    public static void Error(final String tag, String message, Exception e)
    {
        if (BuildConfig.DEBUG)
        {
            Log.e(tag, message, e);
        }
    }

    public static void Info(final String tag, String message)
    {
        if (BuildConfig.DEBUG)
        {
            Log.i(tag, message);
        }
    }

    public static void Info(final String tag, String message, Exception exception)
    {
        if (BuildConfig.DEBUG)
        {
            Log.i(tag, message, exception);
        }
    }

}
