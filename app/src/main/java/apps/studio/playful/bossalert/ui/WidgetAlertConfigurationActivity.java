package apps.studio.playful.bossalert.ui;


import android.app.ListActivity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RemoteViews;
import android.widget.Toast;

import java.util.List;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.logic.WidgetAlertProvider;
import apps.studio.playful.bossalert.models.Sender;

public class WidgetAlertConfigurationActivity extends ListActivity {

    private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    public static final String PREFS_NAME = "apps.studio.playful.bossalert.logic.WidgetAlertProvider";
    public static final String PREFS_PREFIX = "widget_id_";

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // Get the selected group
        String group = (String) l.getItemAtPosition(position);

        // Update the app widget view
        Context context = getApplicationContext();
        saveGroupPref(context, mAppWidgetId, group);

        // Push widget update
        AppWidgetManager widgetManager = AppWidgetManager.getInstance(context);
        WidgetAlertProvider.updateAppWidget(context, widgetManager, mAppWidgetId, group);

        // Return the intent
        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        setResult(RESULT_OK, resultValue);
        finish();

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set default result to cancelled in case all fails
        setResult(RESULT_CANCELED);

        // Screen layout
        setContentView(R.layout.widget_alert_configuration);

        // Get widget information
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // Fail if no widget ID
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

        // Get all current groups the user is subscribed to
        Sender server = Sender.getInstance(getApplicationContext());
        if (server.user != null) {
            if (server.user.getGroups().isEmpty()) {
                // No groups found so just end the activity
                Toast.makeText(
                        getApplicationContext(), "No Groups Found", Toast.LENGTH_SHORT).show();
                finish();
            }
            List<String> groups = server.user.getGroups();
            if (!groups.isEmpty()) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        this, android.R.layout.simple_list_item_1, groups
                );
                setListAdapter(adapter);
            }
        } else {
            // User doesn't exist
            Toast.makeText(getApplicationContext(), "Please run the app before creating a widget",
                    Toast.LENGTH_LONG).show();
            finish();
        }

    }


    public static void saveGroupPref(Context context, int widgetId, String group) {
        // Save group to widget ID in preferences
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putString(PREFS_PREFIX + widgetId, group);
        prefs.commit();
    }

    public static String loadGroupPref(Context context, int widgetId) {
        // Load the widget group from preferences based on widget ID
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String group = prefs.getString(PREFS_PREFIX + widgetId, null);
        if (group == null) {
            group = context.getString(R.string.default_widget_group);

        }

        return group;
    }
}
