package apps.studio.playful.bossalert.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

public class ChatMessage {

    public String group;
    public String senderId;
    public String senderName;
    public HashMap<String, Object> timeStamp;
    public String messageSent;

    /**
     * EMPTY CONSTRUCTOR
     * <p>
     * FireBase requirement
     */
    public ChatMessage() {

    }

    /**
     * DEFAULT CONSTRUCTOR
     *
     * Used to create a new ChatMessage object to store in FireBase
     *
     * @param group         The group the message was sent to
     * @param senderId      The ID of the sender
     * @param senderName    The senders name (to prevent loading more information than we have to)
     * @param messageSent   The actual message that was sentQ
     */
    public ChatMessage(String group, String senderId, String senderName, String messageSent) {
        this.group = group;
        this.senderId = senderId;
        this.messageSent = messageSent;
        HashMap<String, Object> timeStampNow = new HashMap<>();
        timeStampNow.put("timestamp", ServerValue.TIMESTAMP);
        this.timeStamp = timeStampNow;
        this.senderName = senderName;
    }

    /**
     * Gets the server time (long) and converts it to local time (based on devices TimeZone)
     *
     * @param timeStamp Unix timestamp
     * @return          User friendly string representation of the timestamp
     */
    public static String getUserFriendlyTime(long timeStamp)
    {
        // Get local timezone
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();

        // Determine difference in time (if greater than a day, change)
        long timeNow = System.currentTimeMillis();
        long timeDifference = (timeNow / 1000L) - (timeStamp / 1000L);
        int hourDifference = (int) timeDifference / 3600;
        if (hourDifference < 1)
        {
            int minuteDifference = (int) timeDifference / 60;
            return minuteDifference + " minutes ago";
        }
        else if (hourDifference < 24 && hourDifference >= 1)
        {
            return hourDifference + " hours ago";
        }

        // Format the date to match local timezone
        DateFormat sdf = SimpleDateFormat.getDateTimeInstance();
        sdf.setTimeZone(tz);
        return sdf.format(new Date(timeStamp));
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getUserFriendlyDate(String timestamp) {
        String[] splitTimeStamp = timestamp.split(" ");
        return splitTimeStamp[0];
    }

    public String getMessageSent() {
        return messageSent;
    }

    public void setMessageSent(String messageSent) {
        this.messageSent = messageSent;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public HashMap<String, Object> getTimeStamp() {
        return this.timeStamp;
    }

    @Exclude
    public long getTimestampCreatedLong() {
        return (long) timeStamp.get("timestamp");
    }

    @Override
    public String toString() {
        return super.toString();
    }


}
