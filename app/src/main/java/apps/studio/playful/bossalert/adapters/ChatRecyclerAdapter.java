package apps.studio.playful.bossalert.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;
import java.util.Map;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.helpers.LogUtils;
import apps.studio.playful.bossalert.helpers.PreferenceHelper;
import apps.studio.playful.bossalert.models.ChatMessage;
import apps.studio.playful.bossalert.models.ChatUser;
import apps.studio.playful.bossalert.ui.views.ChatBubbleLayout;
import apps.studio.playful.bossalert.ui.views.RoundedImageView;

import static apps.studio.playful.bossalert.helpers.Constants.ANONYMOUS;
import static apps.studio.playful.bossalert.helpers.Constants.BLUE;
import static apps.studio.playful.bossalert.helpers.Constants.ORANGE;
import static apps.studio.playful.bossalert.helpers.Constants.USERS;

public class ChatRecyclerAdapter extends RecyclerView.Adapter<ChatRecyclerAdapter.ViewHolder> {

    public static final String TAG = "ChatRecyclerAdapter";

    private List<ChatMessage> mMessages;
    private Context mContext;
    private LinearLayout.LayoutParams mImageViewLP;
    private Map<String, ChatUser> userCache;

    public ChatRecyclerAdapter(List<ChatMessage> messages, Map<String, ChatUser> userCache,
                               Context context) {
        this.mMessages = messages;
        this.mContext = context;
        this.userCache = userCache;
    }

    @Override
    public ChatRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Load layout
        LinearLayout chatMessageLayout = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_chat_message, parent, false);

        // Create view holder
        ViewHolder viewHolder = new ViewHolder(chatMessageLayout);
        mImageViewLP = viewHolder.getOriginalLayoutParams();

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // Get chat element
        ChatMessage message = mMessages.get(position);

        // Hide the top margin
        holder.chatBubbleMargin.setVisibility(View.GONE);

        // Load data into view holder
        holder.senderNameTV.setText(message.getSenderName());
        holder.messageTV.setText(message.getMessageSent());
        holder.messageTV.setMovementMethod(LinkMovementMethod.getInstance()); /* Allow for links */

        // Create user friendly and time zone friendly time stamp
        String timeStamp = ChatMessage.getUserFriendlyTime(message.getTimestampCreatedLong());
        holder.sendDateTV.setText(timeStamp);

        // Set toggleable time stamp (on click show)
        setToggleableTimestamp(new View[]{
                holder.chatBubbleInnerLayout,
                holder.senderNameTV,
                holder.messageTV
        }, holder.sendDateTV);

        // Set the profile picture
        Bitmap profilePicture = BitmapFactory
                .decodeResource(mContext.getResources(), R.drawable.com_facebook_profile_picture_blank_portrait);
        holder.profilePictureIV.setImageBitmap(profilePicture);

        // Check if the message was from the current user
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        boolean isCurrentSender = userId.equals(message.getSenderId());
        boolean isMessageAnonymous = message.getSenderName().equals(ANONYMOUS);


        if (isCurrentSender && !isMessageAnonymous) {
            // Message is from the current user
            setViewForUser(holder);
        } else {
            // Message is from another user (anonymous or not)
            boolean isAnonymous = isAnonymous(message.senderName);
            boolean showProfilePicture = showProfilePicture(position, isAnonymous, message.senderId);
            setViewForOther(holder, showProfilePicture, message.getSenderId(), isAnonymous);
        }
    }

    /**
     * Set the chat bubble for message sent by the current user
     *
     * @param holder ViewHolder
     */
    private void setViewForUser(ViewHolder holder) {

        // Set the chat bubble to be "bubble" for normal messages
        holder.chatBubbleInnerLayout.setBackground(
                ContextCompat.getDrawable(mContext, R.drawable.bubble));

        // Move the chat bubble to the right
        holder.chatBubbleOuterLayout.setGravity(Gravity.END);

        // Hide the sender name
        holder.senderNameTV.setVisibility(View.GONE);

        // Set the color of the chat bubble to match the theme

        setChatBubbleTheme(holder.chatBubbleInnerLayout);

        // Set the profile picture to invisible to add the correct padding for long messages
        holder.profilePictureIV.setVisibility(View.INVISIBLE);
        holder.profilePictureIV.setLayoutParams(new LinearLayout.LayoutParams(
                mImageViewLP.width + 10, 0
        ));

        // Change the text color
        holder.messageTV.setTextColor(Color.WHITE);
        holder.messageTV.setLinkTextColor(Color.WHITE);
        holder.sendDateTV.setTextColor(Color.WHITE);
    }

    /**
     * Set the chat bubble for a message sent by someone besides the current user
     *
     * @param holder             ViewHolder
     * @param showProfilePicture Whether to display profile picture or not
     * @param senderId           ID of the message sender
     */
    private void setViewForOther(ViewHolder holder, boolean showProfilePicture, String senderId, boolean isAnonymous) {
        // Check whether the profile picture needs to be shown

        if (showProfilePicture) {
            // Profile picture needs to be shown
            holder.profilePictureIV.setVisibility(View.VISIBLE);

            // Show the profile picture for non anonymous users
            if (!isAnonymous) {
                loadProfilePicture(senderId, holder.profilePictureIV);
            }

            holder.profilePictureIV.setLayoutParams(mImageViewLP);
            holder.senderNameTV.setVisibility(View.VISIBLE);

            // Show the margin above the chat bubble
            holder.chatBubbleMargin.setVisibility(View.VISIBLE);

            // Set the chat bubble to be "bubble_start" to signify beginning of string of messages
            holder.chatBubbleInnerLayout.setBackground(
                    ContextCompat.getDrawable(mContext, R.drawable.bubble_start));

        } else {
            // Profile picture needs to be hidden
            holder.senderNameTV.setVisibility(View.GONE);
            holder.profilePictureIV.setVisibility(View.INVISIBLE);

            // Indent the message a little bit
            holder.profilePictureIV.setLayoutParams(new LinearLayout.LayoutParams(
                    mImageViewLP.width + 10, 0));

            // Set the chat bubble to be "bubble" for normal messages
            holder.chatBubbleInnerLayout.setBackground(
                    ContextCompat.getDrawable(mContext, R.drawable.bubble));
        }

        // Move the chat bubble to the right
        holder.chatBubbleOuterLayout.setGravity(Gravity.START);

        // Change the colors of the text
        holder.messageTV.setTextColor(Color.BLACK);
        holder.messageTV.setLinkTextColor(Color.BLACK);
        holder.sendDateTV.setTextColor(Color.BLACK);
        holder.senderNameTV.setTextColor(Color.BLACK);

        // Change the background color to the default gray
        DrawableCompat.setTint(holder.chatBubbleInnerLayout.getBackground(),
                ContextCompat.getColor(mContext, R.color.colorGray));


        // Hide the message timestamp
        holder.sendDateTV.setVisibility(View.GONE);
    }

    /**
     * Set the background color of the chat bubble based on the users selected theme
     *
     * @param chatBubble Inner layout of chat bubble
     */
    private void setChatBubbleTheme(RelativeLayout chatBubble) {

        PreferenceHelper preferenceHelper = new PreferenceHelper(mContext);

        // Get the theme from preferences
        String theme = preferenceHelper.getStringValue(mContext.getString(R.string.key_color_scheme), ORANGE);

        // Depending on the theme, change the chat bubble color
        switch (theme) {
            case ORANGE:
                DrawableCompat.setTint(chatBubble.getBackground(),
                        ContextCompat.getColor(mContext, R.color.defaultPrimary));
                break;
            case BLUE:
                DrawableCompat.setTint(chatBubble.getBackground(),
                        ContextCompat.getColor(mContext, R.color.cyanPrimary));
                break;
        }
    }

    /**
     * Loads the profile image either from cache or gets the URL from Firebase and adds the bitmap
     * to the cache
     *
     * @param userId    The users ID to load the image
     * @param imageView The image view to load the image into
     */
    private void loadProfilePicture(final String userId, final RoundedImageView imageView) {


        // Default all profile pictures to the default blank profile picture
        Picasso.with(mContext)
                .load(R.drawable.com_facebook_profile_picture_blank_portrait)
                .into(imageView);

        ChatUser user = userCache.get(userId);
        if (user != null)
        {
            if (user.getProfilePictureBitmap() != null) {
                imageView.setImageBitmap(user.getProfilePictureBitmap());
            }
            else
            {
                Picasso.with(mContext)
                        .load(user.getProfilePicture())
                        .error(R.drawable.com_facebook_profile_picture_blank_portrait)
                        .into(imageView);
            }
        } else {
            // If not cached, load the URL from Firebase
            DatabaseReference db = FirebaseDatabase.getInstance().getReference();
            db.child(USERS).child(userId).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            // If the user doesn't exist for w/e reason, don't load the image
                            if (dataSnapshot.getValue() != null) {
                                final ChatUser chatUser = dataSnapshot.getValue(ChatUser.class);

                                // Create a new Target to load the image into so we can store it in
                                // the map
                                Target target = new Target() {
                                    @Override
                                    public void onBitmapLoaded(
                                            Bitmap bitmap, Picasso.LoadedFrom from
                                    ) {
                                        chatUser.setProfilePictureBitmap(bitmap);
                                        // Add the image to the cache
                                        userCache.put(userId, chatUser);

                                        // Set the profile picture
                                        imageView.setImageBitmap(bitmap);
                                    }

                                    @Override
                                    public void onBitmapFailed(Drawable errorDrawable) {

                                    }

                                    @Override
                                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                                    }
                                };

                                // Load the image with Picasso
                                Picasso.with(mContext)
                                        .load(chatUser.getProfilePicture())
                                        .error(R.drawable.com_facebook_button_icon_blue)
                                        .into(target);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
        }
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    /**
     * Sets the timestamp to toggle when view is clicked
     *
     * @param views             View to listen for click
     * @param timestampTextView Timestamp view to toggle (dependant on row)
     */
    private void setToggleableTimestamp(View[] views, final TextView timestampTextView) {
        for (View view : views) {

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // If visible, hide
                    if (timestampTextView.getVisibility() == View.VISIBLE) {
                        timestampTextView.setVisibility(View.GONE);
                    } else {
                        timestampTextView.setVisibility(View.VISIBLE);

                    }
                }
            });
        }
    }

    /**
     * Helper method for determining whether or not the profile picture should be show or not.
     * The profile picture should only be shown if the sender is anonymous or it is the first
     * message from the user in a list of messages
     *
     * @param position   Current position of message
     * @param isAnonymous Is the sender anonymous
     * @param senderId   ID of current message sender
     * @return boolean      Show the profile image
     */
    private boolean showProfilePicture(int position, boolean isAnonymous, String senderId) {
        // Check if the sender is anonymous or a previous sender
        boolean isPreviousSender = isPreviousSender(position, senderId);
        return !isPreviousSender || isAnonymous;
    }

    private boolean isAnonymous(String senderName) {
        return senderName.equals(ANONYMOUS);
    }


    /**
     * Check if the previously sent message is from the same user who sent the latest message
     * If this is the case, we will hide the chat head
     *
     * @param position Current position in adapter
     * @param senderId ID of previous sender
     * @return Is the previous sender
     */
    private boolean isPreviousSender(int position, String senderId) {
        // Check that it is not the previous sender
        ChatMessage previousSender = null;
        boolean isPreviousSender = false;
        try {
            previousSender = mMessages.get(position - 1);
        } catch (IndexOutOfBoundsException e) {

        } finally {
            if (previousSender != null) {
                isPreviousSender = previousSender.getSenderId().equals(senderId);
                isPreviousSender = isPreviousSender && !previousSender.getSenderName().equals(ANONYMOUS);
            }
        }

        return isPreviousSender;
    }

    /**
     * ViewHolder object for the recycler view
     */
    static class ViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView profilePictureIV;
        TextView senderNameTV, messageTV, sendDateTV;
        LinearLayout listItemLayout, chatBubbleOuterLayout, chatBubbleMargin;
        ChatBubbleLayout chatBubbleInnerLayout;

        ViewHolder(LinearLayout chatMessageLayout) {
            super(chatMessageLayout);

            // Load all the layout elements
            profilePictureIV = chatMessageLayout.findViewById(R.id.riv_chat_profile_pic);
            senderNameTV = chatMessageLayout.findViewById(R.id.tv_chat_sender);
            messageTV = chatMessageLayout.findViewById(R.id.tv_chat_message);
            sendDateTV = chatMessageLayout.findViewById(R.id.tv_chat_timestamp);
            listItemLayout = chatMessageLayout.findViewById(R.id.ll_chat_outer);
            chatBubbleOuterLayout = chatMessageLayout.findViewById(R.id.ll_chat_bubble);
            chatBubbleInnerLayout = chatMessageLayout.findViewById(R.id.ll_chat_bubble_inner);
            chatBubbleMargin = chatMessageLayout.findViewById(R.id.lv_chat_bubble_margin);

        }

        LinearLayout.LayoutParams getOriginalLayoutParams() {
            return (LinearLayout.LayoutParams) profilePictureIV.getLayoutParams();
        }
    }
}
