package apps.studio.playful.bossalert.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.adapters.GroupManageAdapter;
import apps.studio.playful.bossalert.models.ChatUser;
import apps.studio.playful.bossalert.ui.MainActivity;

import static apps.studio.playful.bossalert.helpers.Constants.FirebaseKeys.TOPICS;
import static apps.studio.playful.bossalert.helpers.Constants.OWNER;
import static apps.studio.playful.bossalert.helpers.Constants.USERS;

public class GroupManagementFragment extends Fragment {

    public static final String TAG = "GroupManagementFragment";

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_CHAT_NAME = "arg_chat_name";
    AdapterView.AdapterContextMenuInfo mMenuInfo;
    //
    //  Layout Members
    //
    private View mView;
    private LayoutInflater mLayoutInflater;
    private ListView mUsersListView;
    //
    //  Group Information
    //
    private String mChatName;
    private Map<String, Boolean> mBanList;
    private List<ChatUser> mUserList;
    private GroupManageAdapter mGroupManageAdapter;
    //
    //  Other
    //
    private Context mContext;

    public GroupManagementFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment GroupManagementFragment.
     */
    public static GroupManagementFragment newInstance(String param1) {
        GroupManagementFragment fragment = new GroupManagementFragment();
        Bundle args = new Bundle();
        args.putString(ARG_CHAT_NAME, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Safely assume that getArguments will never be null
        if (getArguments() != null) {
            mChatName = getArguments().getString(ARG_CHAT_NAME);
        }

        mContext = getActivity().getApplicationContext();
        mBanList = new ArrayMap<String, Boolean>();
        mUserList = new ArrayList<>();

        mGroupManageAdapter = new GroupManageAdapter(mContext, R.layout.list_item_member, mUserList);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Init layout
        mLayoutInflater = inflater;
        mView = mLayoutInflater.inflate(R.layout.fragment_group_management, container, false);

        // Handle Main Activity view modifications
        ((MainActivity) getActivity()).hideFloatingActionButton();
        ((MainActivity) getActivity()).setToolbarTitle("Manage " + mChatName);

        // Load the ListView
        mUsersListView = mView.findViewById(R.id.lv_manage_user);
        mUsersListView.setAdapter(mGroupManageAdapter);
        mUsersListView.setClickable(true);
        registerForContextMenu(mUsersListView);

        loadAllUsers();
        return mView;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId() == R.id.lv_manage_user)
        {
            // Attempt to set global for menu information
            try {
                mMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;

            } catch (ClassCastException e) {
                return;
            }

            int userIndex = mMenuInfo.position;
            ChatUser user = mUserList.get(userIndex);
            boolean isBanned = mBanList.get(user.getId());

            if (isBanned) {
                menu.add(R.string.remove_ban);
            } else {
                menu.add(R.string.ban);
                menu.add(R.string.make_admin);
            }
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        ChatUser user = mUserList.get(mMenuInfo.position);

        if (item.getTitle().equals(getString(R.string.ban))) {
            toggleBan(user.getId(), false);
            Toast.makeText(mContext, "You have banned " + user.getDisplayName(), Toast.LENGTH_SHORT).show();
        } else if (item.getTitle().equals(getString(R.string.remove_ban))){
            toggleBan(user.getId(), true);
            Toast.makeText(mContext, "You have unbanned " + user.getDisplayName(), Toast.LENGTH_SHORT).show();
        } else if (item.getTitle().equals(getString(R.string.make_admin))) {
            makeAdmin(user.getId());
        }

        return true;
    }

    private void loadAllUsers()
    {
        // First get all the active users and the ban list (TODO: Better Implementation)
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        ref.child(TOPICS).child(mChatName).child(USERS).addValueEventListener(
                new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot banFlag : dataSnapshot.getChildren())
                {
                    String key = banFlag.getKey();
                    boolean isBanned = !banFlag.getValue(Boolean.class);
                    mBanList.put(key, isBanned);
                    loadUserInformation(key);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void loadUserInformation(final String userId)
    {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child(USERS).child(userId);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ChatUser user = dataSnapshot.getValue(ChatUser.class);
                if (user != null)
                {
                    user.setId(userId);
                    if (!mUserList.contains(user)) {
                        mUserList.add(user);
                    }

                    sortUserList();
                    mGroupManageAdapter.refresh(mUserList, mBanList);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void toggleBan(String userId, boolean isBanned)
    {
        FirebaseDatabase.getInstance().getReference()
                .child(TOPICS).child(mChatName).child(USERS).child(userId).setValue(isBanned);
    }

    private void makeAdmin(String userId)
    {
        FirebaseDatabase.getInstance().getReference()
                .child(TOPICS).child(mChatName).child(OWNER).setValue(userId);
        this.getActivity().onBackPressed();
    }

    public void sortUserList() {
        Collections.sort(mUserList, new Comparator<ChatUser>() {
            @Override
            public int compare(ChatUser o1, ChatUser o2) {
                if (o1.getId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    return -1;
                } else {
                    return o1.getDisplayName().compareToIgnoreCase(o2.getDisplayName());
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        sortUserList();
        mGroupManageAdapter.refresh(mUserList, mBanList);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
