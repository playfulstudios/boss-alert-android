package apps.studio.playful.bossalert.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.List;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.models.ChatMessage;
import apps.studio.playful.bossalert.models.Group;
import apps.studio.playful.bossalert.models.LastMessage;
import apps.studio.playful.bossalert.ui.views.RoundedImageView;

import static apps.studio.playful.bossalert.helpers.Constants.FirebaseKeys.TOPICS;

public class GroupsArrayAdapter extends ArrayAdapter<Group> {

    public static final String TAG = "GroupsArrayAdapter";

    private List<Group> mGroups;
    private Context mContext;

    public GroupsArrayAdapter(Context context, int resource, List<Group> groups) {
        super(context, resource);

        mContext = context;
        mGroups = groups;
    }

    @Override
    public int getCount() {
        return mGroups.size();
    }

    @Override
    public Group getItem(int position) {
        return mGroups.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {

        final GroupsViewHolder holder;

        // Check if ViewHolder exists already
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_groups, parent, false);

            // Setup the view holder
            holder = new GroupsViewHolder();
            holder.profilePictureIV = convertView.findViewById(R.id.riv_group_profile_pic);
            holder.groupNameTV = convertView.findViewById(R.id.tv_group_list_name);
            holder.lastMessageTV = convertView.findViewById(R.id.tv_group_list_last_msg);
            holder.timeStampTV = convertView.findViewById(R.id.tv_group_list_timestamp);

            // Set convert view tag to the view holder
            convertView.setTag(holder);
        } else {
            // Load existing group view
            holder = (GroupsViewHolder) convertView.getTag();
        }

        // Get all data
        Group group = mGroups.get(position);
        if (group == null) return convertView; // No group so fail

        // Load the data into the viewholder
        holder.groupNameTV.setText(group.getNickName());

        // Load last message details if it exists
        LastMessage lastMessage = group.getLastMessage();
        updateLastMessage(holder, lastMessage, position);

        // Set listener for changing the data
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference().child(TOPICS).child(group.getName());
        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                LastMessage lastMessage = dataSnapshot.getValue(LastMessage.class);
                if (lastMessage != null) {
                    mGroups.get(position).lastMessage = lastMessage;
                    notifyDataSetInvalidated();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        ref.addChildEventListener(listener);

        return convertView;
    }

    private void updateLastMessage(GroupsViewHolder holder, LastMessage lastMessage, int index) {

        String lastMessageText = mContext.getString(R.string.new_group);
        String timeStampText = "No Date Found";
        if (lastMessage != null) {
            if (lastMessage.getLastMessage() != null) {

                lastMessageText = lastMessage.getLastMessage();

                // Safety for older versions that don't have timestamp
                if(lastMessage.getTimeStamp() != null) {
                    timeStampText = ChatMessage.getUserFriendlyTime((long) lastMessage.getTimeStamp()
                            .get("timestamp"));
                }
                else {
                    timeStampText = "";
                }
            }

            // Load the profile image
            Picasso.with(mContext)
                    .load(lastMessage.getUserPicture())
                    .error(R.drawable.com_facebook_profile_picture_blank_portrait)
                    .into(holder.profilePictureIV);
        }

        holder.lastMessageTV.setText(lastMessageText);
        holder.timeStampTV.setText(timeStampText);
    }

    /**
     * ViewHolder for the adapter
     */
    private class GroupsViewHolder
    {
        RoundedImageView profilePictureIV;
        TextView groupNameTV;
        TextView lastMessageTV;
        TextView timeStampTV;
    }


}
