package apps.studio.playful.bossalert.logic;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.ui.WidgetAlertConfigurationActivity;

public class WidgetAlertProvider extends AppWidgetProvider {

    private static final String TAG = "WidgetAlertProvider";

    public static String ACTION_SEND_ALERT = "send_alert";


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        for (int widgetId : appWidgetIds) {
            String group = WidgetAlertConfigurationActivity.loadGroupPref(context, widgetId);
            updateAppWidget(context, appWidgetManager, widgetId, group);
        }
    }

   public static void updateAppWidget(Context context, AppWidgetManager widgetManager, int widgetId,
                                String group) {
        // Construct the remote view object and set info
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.widget_alert);
        remoteViews.setTextViewText(R.id.tv_widget_alert, group);

        // Add on click listener to image button
        Intent intent = new Intent(context, WidgetAlertProvider.class);
        intent.setAction(ACTION_SEND_ALERT);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);

        // Set pending intent for button listener
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, widgetId, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.ib_widget_alert, pendingIntent);

        // Tell the widget manager
        widgetManager.updateAppWidget(widgetId, remoteViews);

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        if (ACTION_SEND_ALERT.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            if (extras != null ) {
                int widgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID);
                String group = WidgetAlertConfigurationActivity.loadGroupPref(context, widgetId);
                NotificationSender sender = new NotificationSender(context);
                sender.widgetSendToGroup(group);
            }
        }
    }
}