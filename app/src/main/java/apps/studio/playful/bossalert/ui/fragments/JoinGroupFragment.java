package apps.studio.playful.bossalert.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.helpers.LogUtils;
import apps.studio.playful.bossalert.models.Group;
import apps.studio.playful.bossalert.models.Password;
import apps.studio.playful.bossalert.models.Sender;
import apps.studio.playful.bossalert.ui.MainActivity;

import static apps.studio.playful.bossalert.helpers.Constants.FirebaseKeys;
import static apps.studio.playful.bossalert.helpers.Constants.UI_TEXT;
import static apps.studio.playful.bossalert.helpers.Constants.USERS;

public class JoinGroupFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "JoinGroupFragment";

    private static String ARG_GROUP_NAME = "arg_group_name";

    private Context mContext;

    private EditText mTopicEditText, mPasswordEditText;

    private Sender mSender;

    private AlertDialog.Builder mDialogBuilder;
    private ProgressDialog mProgressDialog;

    public static JoinGroupFragment shareGroupInstance(String group)
    {
        JoinGroupFragment joinGroupFragment = new JoinGroupFragment();

        // Add arguments of the shared group
        Bundle args = new Bundle();
        args.putString(ARG_GROUP_NAME, group);
        joinGroupFragment.setArguments(args);

        return joinGroupFragment;
    }


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_topic, container, false);

        mContext = this.getActivity().getWindow().getContext();
        mDialogBuilder = new AlertDialog.Builder(mContext);

        // Get all the senderId information
        mSender = Sender.getInstance(mContext);

        // Get the edit texts containing name and password
        mTopicEditText = view.findViewById(R.id.et_join_group_name);
        mTopicEditText.requestFocus();
        mPasswordEditText = view.findViewById(R.id.et_join_group_password);
        try
        {
            Bundle bundle = getArguments();
            String sharedGroup = bundle.getString(ARG_GROUP_NAME);
            if (sharedGroup != null)
            {
                mTopicEditText.setText(sharedGroup, TextView.BufferType.EDITABLE);
                mPasswordEditText.requestFocus();
            }
        }
        catch (NullPointerException ignored)
        {

        }

        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        Button joinGroupButton = view.findViewById(R.id.btn_join_group);
        joinGroupButton.setOnClickListener(this);

        ((MainActivity) getActivity()).hideFloatingActionButton();
        ((MainActivity) getActivity()).setToolbarTitle("Join Group");

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_join_group:
                String password = mPasswordEditText.getText().toString();
                String topic = mTopicEditText.getText().toString();

                boolean topicValid = validateGroupName();
                boolean passwordValid = validatePassword();

                if (topicValid && passwordValid) {
                    //If fields valid, join group
                    subscribeToTopic(mTopicEditText.getText().toString().replaceAll("\\s+", ""), mPasswordEditText.getText().toString());
                } else {
                    // One or more fields is invalid so handle the errors
                    if (!topicValid) {
                        // Topic is invalid so let the user know by setting an error string
                        mTopicEditText.setError(getString(R.string.err_invalid_group));
                    }
                    if (!passwordValid) {
                        // Password is invalid so let the user know by setting an error string
                        mPasswordEditText.setError(getString(R.string.err_invalid_password));
                    }
                }
        }
    }

    /**
     * Handles joining a specific topic. If it exists already, the passwords that the user has entered
     * and the stored versions are compared. If the topic doesn't exist, create it and then join it
     *
     * @param topic    Name of the topic the user wants to join
     * @param password Password for the topic
     */
    private void subscribeToTopic(final String topic, String password) {

        // Load the progress dialog
        mProgressDialog = ProgressDialog.show(mContext, "Joining Group", "Attempting to validate group", true);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference(FirebaseKeys.TOPICS);

        // Get the owner of the topic
        String owner = FirebaseAuth.getInstance().getCurrentUser().getUid();

        // Create basic model for FireBase to handle the topic
        final Group currentTopic = new Group(topic, owner, password);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(topic)) {

                    final Group group = dataSnapshot.child(topic).getValue(Group.class);
                    if (group.isLocked())
                    {
                        Toast.makeText(mContext, "Failed to join group", Toast.LENGTH_LONG).show();
                        closeKeyboard();
                        mProgressDialog.dismiss();
                        openGroupFragment();
                        return;
                    }

                    // Group already exists, create dialog to confirm join
                    mDialogBuilder.setMessage("Group already exists. Would you like to join instead?");
                    mDialogBuilder.setPositiveButton(UI_TEXT.JOIN, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (Password.verifyUserPassword(
                                    currentTopic.getPassword(),
                                    group.getSalt(),
                                    group.getPassword()))
                            {
                                FirebaseDatabase database = FirebaseDatabase.getInstance();
                                DatabaseReference dbRef = database.getReference().child(FirebaseKeys.TOPICS)
                                        .child(currentTopic.getName()).child(USERS)
                                        .child(mSender.user.getId());

                                dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        try {
                                            boolean isBanned = !dataSnapshot.getValue(Boolean.class);

                                            // If user entered password is the same as the one retrieved from Firebase, join topic
                                            if (!isBanned) {
                                                joinTopic(topic);
                                            } else {
                                                Toast.makeText(mContext,
                                                        "Unable to join " + currentTopic.getName(),
                                                        Toast.LENGTH_LONG).show();
                                                mProgressDialog.dismiss();
                                                closeKeyboard();
                                            }
                                        } catch (NullPointerException e) {
                                            // Use not part of topic yet so join
                                            joinTopic(topic);
                                        }

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });



                            } else {
                                // User entered an invalid password
                                mPasswordEditText.setError(getString(R.string.err_password_does_not_match));
                                mProgressDialog.dismiss();
                                dialog.cancel();
                            }
                        }
                    });
                    // Cancel the dialog
                    mDialogBuilder.setNegativeButton(UI_TEXT.CANCEL, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mProgressDialog.dismiss();
                            // password
                            dialog.cancel();
                        }
                    });
                    mDialogBuilder.show();
                } else {
                    // Topic is new so send it to Firebase along with the hashed version of the
                    currentTopic.setPassword(Password.getHashedPassword(currentTopic.password, currentTopic.salt));
                    ref.child(topic).setValue(currentTopic);
                    joinTopic(topic);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    /**
     * Handles validation of the password input.
     *
     * @return Whether the password is valid or not
     */
    private boolean validatePassword() {
        String password = mPasswordEditText.getText().toString();
        boolean valid = false;

        if (password.length() >= 4 && password.length() <= 32) {
            valid = true;
        }

        return valid;
    }

    /**
     * Handles validation of the group name
     *
     * @return Whether the group name is valid or not
     */
    private boolean validateGroupName() {
        String groupName = mTopicEditText.getText().toString();
        boolean valid = false;

        if (groupName.length() >= 4 && groupName.length() <= 32) {
            valid = true;
        }

        return valid;
    }

    /**
     * Handles final joining process of topic by showing the user output and subscribing to Firebase
     * topic
     *
     * @param topic
     */
    private void joinTopic(String topic) {
        FirebaseMessaging.getInstance().subscribeToTopic(topic);
        if (mSender == null) {
            LogUtils.Error(TAG, "Could not save token. Missing senderId ID");
        } else {
            addToUserList(topic);
            mSender.user.addGroup(topic);

            // If this is the first topic, add it to the favourites
            if (mSender.user.getFavouriteGroup() == null)
            {
                mSender.user.setFavouriteGroup(topic);
            }

            mSender.save();
        }

        // Create a toast to notify the user that they have joined the topic
        mProgressDialog.dismiss();
        Toast.makeText(mContext, "Successfully joined " + topic,
                Toast.LENGTH_SHORT).show();
        closeKeyboard();


        // Take the user back to the groups fragment
        openGroupFragment();
    }

    private void closeKeyboard() {
        // Close the keyboard
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mTopicEditText.getWindowToken(), 0);
    }

    private void openGroupFragment() {
        Fragment groupsFragment = new GroupsFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, groupsFragment).addToBackStack(null).commit();
    }

    /**
     * Adds user to list of users in FireBase
     *
     * @param topic topic name
     */
    private void addToUserList(String topic) {
        String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        // Create FireBase reference to the topic
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference(FirebaseKeys.TOPICS).child(topic);

        // According to FireBase guidelines, we should just store true when there is no key-value
        ref.child(USERS).child(userID).setValue(true);
    }

    public interface ShareListener
    {
        void JoinSharedGroup(String group);
    }

}
