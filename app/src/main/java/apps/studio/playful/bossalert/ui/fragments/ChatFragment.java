package apps.studio.playful.bossalert.ui.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.adapters.ChatRecyclerAdapter;
import apps.studio.playful.bossalert.helpers.LogUtils;
import apps.studio.playful.bossalert.helpers.PreferenceHelper;
import apps.studio.playful.bossalert.logic.NotificationSender;
import apps.studio.playful.bossalert.models.ChatMessage;
import apps.studio.playful.bossalert.models.ChatUser;
import apps.studio.playful.bossalert.models.Group;
import apps.studio.playful.bossalert.models.Sender;
import apps.studio.playful.bossalert.ui.MainActivity;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_DRAGGING;
import static android.support.v7.widget.RecyclerView.SCROLL_STATE_SETTLING;
import static android.view.View.GONE;
import static apps.studio.playful.bossalert.helpers.Constants.FirebaseKeys.NICKNAME;
import static apps.studio.playful.bossalert.helpers.Constants.BLUE;
import static apps.studio.playful.bossalert.helpers.Constants.UI_TEXT;
import static apps.studio.playful.bossalert.helpers.Constants.LOCKED;
import static apps.studio.playful.bossalert.helpers.Constants.MESSAGES;
import static apps.studio.playful.bossalert.helpers.Constants.MESSAGE_SOURCE_CHAT;
import static apps.studio.playful.bossalert.helpers.Constants.ORANGE;
import static apps.studio.playful.bossalert.helpers.Constants.OWNER;
import static apps.studio.playful.bossalert.helpers.Constants.FirebaseKeys.TOPICS;
import static apps.studio.playful.bossalert.helpers.Constants.USERS;

public class ChatFragment extends Fragment {

    public static final String TAG = "ChatFragment";

    private static String ARG_CHAT_NAME = "arg_chat_name";
    private static int MESSAGE_INCREMENT = 20; // Only load n messages

    //
    //  Layout Properties
    //
    private View mView;
    private Context mContext;
    private LinearLayout mChatInfoLayout;
    private TextView mChatInfoTV;
    private EditText mMessageSendET;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;


    //
    //  Properties of chat loading
    //
    private int mChatPage;
    private List<ChatMessage> mMessageList;
    private Map<String, ChatUser> mUserCache;
    private String mChatName;
    private DatabaseReference mDbReference;
    private Group mGroupInfo;
    private boolean mIsChatOwner;

    //
    // Other
    //
    private NotificationSender mNotificationSender;
    private ChatRecyclerAdapter mChatAdapter;
    private boolean mIsConnected;
    private boolean mCanScroll;
    private boolean mIsScrollingUp;
    private Sender mSender;
    private OnManageListener mListener;
    private boolean mReachedTop;

    public static ChatFragment newInstance(String chatName)
    {
        ChatFragment chatFragment = new ChatFragment();

        // Add arguments to get information about specific chat
        Bundle args = new Bundle();
        args.putString(ARG_CHAT_NAME, chatName);
        chatFragment.setArguments(args);

        return chatFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get the chat index from the bundle
        Bundle bundle = getArguments();
        mChatName = bundle.getString(ARG_CHAT_NAME);

        mContext = this.getActivity().getWindow().getContext();

        // Initialize the data
        mCanScroll = false;
        mIsScrollingUp = false;
        mIsConnected = true;
        mChatPage = 1;
        mIsChatOwner = false;

        mSender = Sender.getInstance(mContext);

        mMessageList = new ArrayList<>();
        mUserCache = new ArrayMap<>();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mDbReference = database.getReference(MESSAGES)
                .child(mChatName)
                .child(MESSAGES);
        mDbReference.keepSynced(true);

        mNotificationSender = new NotificationSender(mContext);
        manageBanStatus();

        getGroupInfo();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_chat, container, false);

        // General fragment creation
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).hideFloatingActionButton();
        ((MainActivity) getActivity()).setToolbarTitle(mChatName);

        // Setup the layout
        mChatInfoLayout = mView.findViewById(R.id.lv_chat_notification);
        mChatInfoTV = mView.findViewById(R.id.tv_chat_notification);
        mChatInfoLayout.setVisibility(View.GONE);

        mMessageSendET = mView.findViewById(R.id.et_message);
        initMessageSendListener();

        initRecyclerView();
        initChatListener();

        // Load the information
        checkUserOnline();

        // Update the UI theme
        setUITheme();

        return mView;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        if (mIsChatOwner) {
            menu.clear();
            inflater.inflate(R.menu.menu_admin_chat, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_change_nick) {
            setGroupNickName();
        } else if (id == R.id.action_manage_users) {
            mListener.onManageSelected(mChatName);
        } else if (id == R.id.action_delete_group) {
            deleteGroup();
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Initialize the view and the scrolling events
     */
    private void initRecyclerView()
    {

        // Load the recycler view
        mLayoutManager = new LinearLayoutManager(mContext);
        mChatAdapter = new ChatRecyclerAdapter(mMessageList, mUserCache, mContext);
        mRecyclerView = mView.findViewById(R.id.rv_chat);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mChatAdapter);

        // Listen for scroll events to load the next page of messages
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == SCROLL_STATE_DRAGGING || newState == SCROLL_STATE_SETTLING) {

                    // Get first visible item in view and compare it to the actual first index
                    int firstVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager())
                            .findFirstCompletelyVisibleItemPosition();

                    // Check if the user is at the top of the view
                    if (firstVisibleItem == 0) {

                        // Check if the user is scrolling up and is allowed to scroll
                        if (mCanScroll && mIsScrollingUp && !mReachedTop) {

                            // Set a message letting user know messages are loading
                            setChatInfoMessage("Loading...",
                                    ContextCompat.getColor(mContext, R.color.blue_500));

                            // Create timeout event to prevent constantly updating
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mChatPage += 1;
                                    loadPage();
                                }
                            }, 500);

                            // Disable scroll until page has loaded
                            mCanScroll = false;
                        }
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // Keep track of whether the user is scrolling up
                mIsScrollingUp = dy < 0;
            }
        });
    }

    /**
     *  Setup listener for the send message button
     */
    private void initMessageSendListener()
    {
        Button sendButton = mView.findViewById(R.id.btn_send_message);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String message = mMessageSendET.getText().toString().trim();

            if (validateMessage(message))
            {
                // Send a notification if the message isn't empty
                mNotificationSender.sendMessageToTopic(mChatName, message,
                        MESSAGE_SOURCE_CHAT);

                // Clear the message
                mMessageSendET.setText("");
            }
            }
        });
    }

    private boolean validateMessage(String message) {
        boolean valid = true;
        if (message.isEmpty()) {
            mMessageSendET.setError("Can't send an empty message");
            valid = false;
        }

        return valid;
    }

    /**
     *  Add a new chat event listener (only on the last items) and add the new messages to view
     */
    private void initChatListener() {
        mDbReference.limitToLast(MESSAGE_INCREMENT).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                // Ensure message actually exists
                if (dataSnapshot == null && dataSnapshot.getValue() == null)
                    return;

                // Load then new chat message
                try {
                    ChatMessage message = dataSnapshot.getValue(ChatMessage.class);

                    if (!mMessageList.contains(message)) {
                        message.setMessageSent(message.getMessageSent());
                        mMessageList.add(message);
                        mChatAdapter.notifyItemChanged(mMessageList.size());
                        mChatAdapter.notifyDataSetChanged();

                        // Scroll to new message
                        // Not sure if this is a good idea
                        mRecyclerView.getLayoutManager()
                                .smoothScrollToPosition(mRecyclerView, null, mMessageList.size());

                        // Increment the page on overflow
                        if (mMessageList.size() > mChatPage * MESSAGE_INCREMENT)
                        {
                            mChatPage += 1;
                        }
                    }
                } catch (DatabaseException e) {
                    LogUtils.Error(TAG, "Invalid data format", e);
                }



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Allow the user to scroll after a certain period of time
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCanScroll = true;
            }
        }, 500);
    }

    /**
     * Loads a new page of chat messages to the view
     */
    private void loadPage()
    {
        ChatMessage lastMessage = mMessageList.get(mMessageList.size() - 1);

        // Check that we have messages loaded
        if (lastMessage == null)
            return;

        // Keep track of where to add the messages
        final int[] addPoint = {0};


        // Query database for new messages
        mDbReference.limitToLast(mChatPage * MESSAGE_INCREMENT)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot child : dataSnapshot.getChildren())
                {
                    ChatMessage message = child.getValue(ChatMessage.class);
                    if (!mMessageList.contains(message)) {

                        // Add the new message to the specific index (before the newer messages)
                        mMessageList.add(addPoint[0], message);
                        mChatAdapter.notifyItemChanged(addPoint[0]);
                        mChatAdapter.notifyDataSetChanged();
                        addPoint[0] += 1;

                        // Scroll to the new messages
                        mRecyclerView.getLayoutManager().smoothScrollToPosition(mRecyclerView, null, addPoint[0]);

                    }
                }

                if (addPoint[0] < MESSAGE_INCREMENT) {
                    // No more messages loaded so must be at the top
                    mReachedTop = true;
                }

                // Hide the loading notifications
                mChatInfoLayout.setVisibility(GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // Create handler to re-enable scrolling
        Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCanScroll = true;
            }
        }, 500);

    }


    /**
     *  Gets the groups information for later use
     */
    private void getGroupInfo() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = database.getReference()
                .child(TOPICS).child(mChatName).child(OWNER);

        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String ownerId = dataSnapshot.getValue(String.class);
                mIsChatOwner = ownerId.equals(mSender.user.getId());

                try {
                    getActivity().invalidateOptionsMenu();
                } catch (NullPointerException e) {
                    LogUtils.Error(TAG, "Invalid options menu", e);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void manageBanStatus()
    {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference dbRef = database.getReference().child(TOPICS)
                .child(mChatName).child(USERS).child(mSender.user.getId());
        dbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean isBanned = false;
                try
                {
                    isBanned = !dataSnapshot.getValue(Boolean.class);
                }
                catch (NullPointerException e)
                {
                    LogUtils.Error(TAG, "Failed to read in boolean for is banned. Defaulting to not " +
                            "banned as relying on old data schema.");
                }


                if (isBanned)
                {
                    // Notify the user they have been banned
                    Toast.makeText(mContext, "You have been banned from " + mChatName,
                            Toast.LENGTH_LONG).show();

                    // Update the current users settings and remove the banned group
                    List<String> currentGroups = mSender.user.getGroups();
                    currentGroups.remove(mChatName);
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(mChatName);
                    mSender.user.setGroups(currentGroups);

                    // Return them to the groups page
                    getActivity().onBackPressed();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Sets the top bar to notify the user of their current status
     *
     * @param message Message to display
     * @param backgroundColor Background color of message
     */
    private void setChatInfoMessage(String message, int backgroundColor)
    {
        mChatInfoTV.setText(message);
        mChatInfoLayout.setBackgroundColor(backgroundColor);
        mChatInfoLayout.setVisibility(View.VISIBLE);
    }

    /**
     * Checks if the user is connected to Firebase and displays a warning message if the user is
     * disconnected at any point
     */
    private void checkUserOnline()
    {

        // User Firebase info to check if the user is online
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(".info/connected");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = dataSnapshot.getValue(Boolean.class);

                if (connected) {
                    // Check if the user was previously disconnected
                    if (!mIsConnected) {
                        setChatInfoMessage(UI_TEXT.CONNECTED,
                                ContextCompat.getColor(mContext, R.color.colorSuccess));

                        // Set delay to hide info message
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mChatInfoLayout.setVisibility(View.GONE);
                            }
                        }, 500);

                        mIsConnected = true;
                    }
                } else {
                    // User not connected to keep message up until this changes
                    setChatInfoMessage(UI_TEXT.DISCONNECTED,
                            ContextCompat.getColor(mContext, R.color.colorFail));
                    mIsConnected = false;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Updates the ui color scheme
     */
    private void setUITheme() {

        // UI elements to update
        Button messageSendButton = mView.findViewById(R.id.btn_send_message);
        PreferenceHelper preferenceHelper = new PreferenceHelper(mContext);

        // Check the current theme
        String theme = preferenceHelper.
                getStringValue(mContext.getString(R.string.key_color_scheme), ORANGE);

        // Update the UI to match the theme
        switch (theme)
        {
            case ORANGE:
                DrawableCompat.setTint(messageSendButton.getBackground(),
                        ContextCompat.getColor(mContext, R.color.defaultPrimary));
                break;
            case BLUE:
                DrawableCompat.setTint(messageSendButton.getBackground(),
                        ContextCompat.getColor(mContext, R.color.cyanPrimary));
                break;
        }
    }

    private void deleteGroup() {
        if (mIsChatOwner)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                    .setTitle("Delete Group")
                    .setMessage("Are you sure you want to delete the group? This cannot be undone")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteGroupOnFirebase();
                                    // Open Groups Fragment
                                    GroupsFragment groupsFragment = new GroupsFragment();
                                    getFragmentManager().beginTransaction()
                                            .replace(R.id.content, groupsFragment, GroupsFragment.TAG)
                                            .commit();
                                }
                            }

                    )
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //
                        }
                    });

            builder.show();
        }
    }

    private void deleteGroupOnFirebase() {
        if (mIsChatOwner)
        {
            // Set the group to locked
            DatabaseReference database = FirebaseDatabase.getInstance().getReference();
            database.child(TOPICS).child(mChatName).child(LOCKED).setValue(true);
        }
    }

    /**
     * Sets the group nickname locally and in Firebase
     * The nickname is not the topic name but just a way to customize the group name without
     * messing with the Firebase topic etc.
     */
    private void setGroupNickName() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
                .setTitle("Set Group Nickname")
                .setMessage("Change the nickname of the group. This will not change the group id");

        final EditText nickNameInput = new EditText(mContext);
        nickNameInput.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(nickNameInput);

        builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String nickName = nickNameInput.getText().toString();
                if (nickName.length() < 4) {
                    nickNameInput.setError("Nickname must be 4 characters or more");
                } else {
                    setGroupNickNameOnFirebase(nickName);
                    dialogInterface.dismiss();
                    Toast.makeText(mContext, "Set group nickname to " + nickName,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.show();
    }

    /**
     * Update the nickname on Firebase
     * @param nickname
     */
    private void setGroupNickNameOnFirebase(String nickname) {
        if (mIsChatOwner) {
            DatabaseReference database = FirebaseDatabase.getInstance().getReference();
            database.child(TOPICS).child(mChatName).child(NICKNAME).setValue(nickname);
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnManageListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                + " must implement OnManageListener");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mRecyclerView.setAdapter(null);
    }

    public interface OnManageListener
    {
        void onManageSelected(String groupName);
    }


}
