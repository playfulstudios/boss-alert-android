package apps.studio.playful.bossalert.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.helpers.PreferenceHelper;
import apps.studio.playful.bossalert.models.Sender;
import apps.studio.playful.bossalert.models.Subscription;
import apps.studio.playful.bossalert.models.User;
import apps.studio.playful.bossalert.ui.fragments.ChatFragment;
import apps.studio.playful.bossalert.ui.fragments.GroupManagementFragment;
import apps.studio.playful.bossalert.ui.fragments.GroupsFragment;
import apps.studio.playful.bossalert.ui.fragments.JoinGroupFragment;
import apps.studio.playful.bossalert.ui.fragments.SettingsFragment;

import static apps.studio.playful.bossalert.helpers.Constants.BLUE;
import static apps.studio.playful.bossalert.helpers.Constants.ORANGE;
import static apps.studio.playful.bossalert.helpers.Constants.SUBSCRIPTIONS;
import static apps.studio.playful.bossalert.helpers.Constants.SUBSCRIPTION_EXPIRY;
import static apps.studio.playful.bossalert.helpers.Constants.USERS;
import static apps.studio.playful.bossalert.models.Subscription.INFINITE_SUBSCRIPTION;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        GroupsFragment.OnGroupSelectedListener,
        ChatFragment.OnManageListener,
        JoinGroupFragment.ShareListener {

    public final static String TAG = "UI.MainActivity";
    Menu mMenu;
    private FloatingActionButton fab;
    private Toolbar toolbar;
    private String userId;
    private boolean initialLoad = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the appropriate theme
        setTheme();

        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Facebook tracking
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        // Load UI elements
        loadFAB();
        showFloatingActionButton();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                hideSoftKeyboard();
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        updateNavHeader();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            userId = user.getUid();

            // Get user information from Database
            ref.child(USERS).child(userId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot.exists()) {
                        Sender sender = Sender.getInstance(getApplicationContext());
                        sender.setUser(dataSnapshot.getValue(User.class));

                        // Check if first time loading data
                        if (!initialLoad) {
                            getInitialFragment();
                        }
                    } else {
                        // User doesn't exist in FireBase so load Login Activity where data can be
                        // created
                        loadLoginActivity();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Cancelled so load login activity
                    loadLoginActivity();
                }
            });

            // Check for current subscription
            ref.child(SUBSCRIPTIONS).child(userId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    long currentSubscription = preferences.getLong(SUBSCRIPTION_EXPIRY, 0);

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                        Subscription subscription = snapshot.getValue(Subscription.class);
                        long currentTime = System.currentTimeMillis();
                        long subscriptionEndTime = subscription.getSubscriptionEndTime();

                        if (subscriptionEndTime > currentSubscription)
                        {
                            preferences.edit().putLong(SUBSCRIPTION_EXPIRY, subscriptionEndTime).apply();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
            loadLoginActivity();
        }
    }

    private void getInitialFragment() {

        // First check if the user was invited via a link
        Intent intent = getIntent();
        String action = intent.getAction();
        Uri data = intent.getData();
        if (data != null) {
            String[] splitData = data.toString().split("/");
            String groupToJoin = splitData[splitData.length - 1];
            JoinSharedGroup(groupToJoin);
            return;
        }

        // Check if chat id passed
        String chatArg = getIntent().getStringExtra("chatFragment");
        if (chatArg != null) {

            // Check if user is subscribed before opening chat
            if (isUserSubscribed()) {
                ChatFragment chatFragment = ChatFragment.newInstance(chatArg);
                loadFragment(chatFragment, false, ChatFragment.TAG);
            } else {
                loadGroupsFragment(false); // Load the default window
            }
        } else {
            loadGroupsFragment(false); // Load the default window
        }
        initialLoad = true;
    }

    private void updateNavHeader()
    {
        // Set theme
        PreferenceHelper preferenceHelper = new PreferenceHelper(getApplicationContext());
        String theme = preferenceHelper.getStringValue(getString(R.string.key_color_scheme), ORANGE);

        NavigationView navView = findViewById(R.id.nav_view);
        View header = navView.getHeaderView(0);
        ImageView headerImage = header.findViewById(R.id.iv_nav_header);
        LinearLayout background = header.findViewById(R.id.lv_header_bg);
        switch (theme)
        {
            case ORANGE:
                headerImage.setImageResource(R.drawable.panel_image_orange);
                background.setBackgroundColor(getResources().getColor(R.color.orange_header));
                break;
            case BLUE:
                headerImage.setImageResource(R.drawable.panel_image_cyan);
                background.setBackgroundColor(getResources().getColor(R.color.cyan_header));
                break;
        }
    }


    /**
     * Sets the current theme
     * Required to be called first in MainActivity.onCreate()
     */
    private void setTheme() {
        // Set theme
        PreferenceHelper preferenceHelper = new PreferenceHelper(getApplicationContext());
        String theme = preferenceHelper.getStringValue(getString(R.string.key_color_scheme), ORANGE);

        switch (theme)
        {
            case ORANGE:
                this.setTheme(R.style.default_theme);
                break;
            case BLUE:
                this.setTheme(R.style.cyan_theme);
                break;
        }
    }

    /**
     * Loads the login activity
     */
    private void loadLoginActivity()
    {
        Intent loginIntent = new Intent(this, apps.studio.playful.bossalert.ui.LoginActivity.class);
        startActivity(loginIntent);
        finish();
    }

    /**
     * Initialises the Floating Action Button
     * TODO: Remove FAB
     */
    private void loadFAB() {
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFragment(new JoinGroupFragment(), true, JoinGroupFragment.TAG);
            }
        });
    }

    /**
     * Logs the user out of whatever service they are signed in with and loads the login activity
     */
    private void logout() {

        // Destroy all subscriptions
        Sender sender = Sender.getInstance(getApplicationContext());
        if (sender != null)
        {
            // Save to firebase
            sender.save();


            for (String group : sender.user.getGroups())
            {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(group);
            }
        }


        // Third part house keeping
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        loadLoginActivity();
        finish();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.mMenu = menu;

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            loadFragment(new SettingsFragment(), true, SettingsFragment.TAG);
        } else if (id == R.id.action_change_nick) {
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_manage) {
            // Load the settings (preference) fragment
            loadFragment(new SettingsFragment(), true, SettingsFragment.TAG);
        }
        else if (id == R.id.nav_home)
        {
            // Load groups fragment
            loadGroupsFragment(true);
        }
        else if (id == R.id.nav_share)
        {
            // Share topic
            shareApp();
        }
        else if (id == R.id.nav_signout) {
            logout();
        }
        else if (id == R.id.nav_join_group){
            loadFragment(new JoinGroupFragment(), true, JoinGroupFragment.TAG);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Share the group
     */
    private void shareApp() {
        Intent shareTopicIntent = new Intent(Intent.ACTION_SEND);
        shareTopicIntent.setType("text/plain");
        shareTopicIntent.putExtra(Intent.EXTRA_SUBJECT, "Check out Boss Alert");
        shareTopicIntent.putExtra(Intent.EXTRA_TEXT, "Be alerted when your boss appears. Check out <LINK HERE>");
        startActivity(Intent.createChooser(shareTopicIntent, "Share via"));
    }

    /**
     * Load a new fragment to view
     * @param fragment          Fragment to load
     * @param addToBackStack    Add to back stack
     * @param tag               Tag to load apply
     */
    public void loadFragment(Fragment fragment, boolean addToBackStack, String tag)
    {
        // Begin transaction
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, fragment, tag);

        // Add to back stack if required
        if (addToBackStack)
        {
            transaction.addToBackStack(tag);
        }

        // Finish transaction
        transaction.commit();
    }

    /**
     * Load the group fragment. Requires special function to handle TAG finding
     * @param addToBack Add to back stack
     */
    private void loadGroupsFragment(boolean addToBack) {
        GroupsFragment groupsFragment = (GroupsFragment) getSupportFragmentManager().findFragmentByTag(GroupsFragment.TAG);

        // Ensure the fragment doesn't already exist
        if (groupsFragment == null) {
            groupsFragment = GroupsFragment.newInstance();
        }

        loadFragment(groupsFragment, addToBack, GroupsFragment.TAG);
    }

    //TODO: Replace with toggle
    public void showFloatingActionButton() {
        fab.show();
    }

    public void hideFloatingActionButton() {
        fab.hide();
    }

    /**
     * Set the text on the action bar (at the top of screen)
     * @param title Title
     */
    public void setToolbarTitle(String title) {
        toolbar.setTitle(title);
    }

    /**
     * Hide the keyboard when required
     */
    public void hideSoftKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Sender sender = Sender.getInstance(getApplication().getApplicationContext());
        sender.save();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Sender sender = Sender.getInstance(getApplication().getApplicationContext());
        sender.save();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    /**
     * Checks the preferences for an active subscription. This should have been updated on
     * MainActivity call so should be a fairly valid way of checking
     *
     * @return Whether the use is subscribed or not depending on the preferences
     */
    public boolean isUserSubscribed() {
        boolean isSubbed = false;

        PreferenceHelper prefHelper = new PreferenceHelper(getApplicationContext());
        long currentSubscription = prefHelper.getLongValue(SUBSCRIPTION_EXPIRY, 0);
        long currentTime = System.currentTimeMillis();

        if (currentSubscription == INFINITE_SUBSCRIPTION || currentSubscription > currentTime) {
            isSubbed = true;
        }

        return isSubbed;
    }

    /**
     * Shows the dialog message letting the user know they can't access a specific feature
     */
    private void showNotPremiumDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Premium Feature");
        alertDialog.setMessage("Please subscribe to use this feature");
        alertDialog.setPositiveButton("Subscribe", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO: Add subscription placeholder
                String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                long currentTime = System.currentTimeMillis();
                Subscription subscription = new Subscription(userId, currentTime, Subscription.SUBSCRIPTION_TYPES.LIFETIME);

                DatabaseReference database = FirebaseDatabase.getInstance().getReference();
                database.child(SUBSCRIPTIONS).child(userId).push().setValue(subscription);

                long expiryTime = subscription.getSubscriptionEndTime();

                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                sharedPref.edit().putLong(SUBSCRIPTION_EXPIRY, expiryTime).commit();
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Dismiss dialog
            }
        });
        alertDialog.show();
    }

    /**
     * Handles the group being selected in the GroupsFragment
     *
     * @param chatName The name of the group the user is opening
     */
    @Override
    public void onGroupSelected(String chatName) {
        if (isUserSubscribed()) {
            ChatFragment chatFragment = ChatFragment.newInstance(chatName);
            loadFragment(chatFragment, true, ChatFragment.TAG);
        } else {
            showNotPremiumDialog();
        }
    }

    @Override
    public void onManageSelected(String chatName) {
        GroupManagementFragment managementFragment = GroupManagementFragment.newInstance(chatName);
        loadFragment(managementFragment, true, GroupManagementFragment.TAG);
    }

    @Override
    public void JoinSharedGroup(String group) {
        JoinGroupFragment groupFragment = JoinGroupFragment.shareGroupInstance(group);
        loadFragment(groupFragment, false, JoinGroupFragment.TAG);
    }
}
