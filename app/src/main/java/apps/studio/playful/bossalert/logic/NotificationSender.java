package apps.studio.playful.bossalert.logic;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Vibrator;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.HashMap;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.helpers.Constants;
import apps.studio.playful.bossalert.helpers.PreferenceHelper;
import apps.studio.playful.bossalert.models.ChatMessage;
import apps.studio.playful.bossalert.models.LastMessage;
import apps.studio.playful.bossalert.models.Sender;

import static apps.studio.playful.bossalert.helpers.Constants.MESSAGE_SOURCE_ALERT;
import static apps.studio.playful.bossalert.helpers.Constants.Notification;
import static apps.studio.playful.bossalert.helpers.Constants.LAST_MESSAGE;
import static apps.studio.playful.bossalert.helpers.Constants.MESSAGES;
import static apps.studio.playful.bossalert.helpers.Constants.FirebaseKeys.TOPICS;

public class NotificationSender {

    private Sender server;
    private Context mContext;
    private PreferenceHelper mPreferenceHelper;

    public NotificationSender(Context context) {
        mContext = context;
        server = Sender.getInstance(mContext);
        mPreferenceHelper = new PreferenceHelper(mContext);
    }

    /**
     * Gets the users display name
     * Default: "Anonymous"
     *
     * @return display name
     */
    private String getDisplayName() {
        String displayName = "Anonymous";

        // If the user has chosen to display their name
        if (mPreferenceHelper.getNameIsDisplayed()) {
            Sender sender = Sender.getInstance(mContext);
            displayName = sender.user.getDisplayName();
        }

        return displayName;
    }

    /**
     * Handles notification sent via the widget
     */
    public void widgetSendToFavouriteGroup() {

        widgetSendToGroup(mContext.getString(R.string.default_widget_group));
    }

    public void widgetSendToGroup(String group) {

        // Check if we are sending to favourite or not
        if (group.equals(mContext.getString(R.string.default_widget_group))) {
            group = server.user.getFavouriteGroup();
        }

        // Ensure that we have valid data
        if (server.user != null && !server.user.getGroups().isEmpty() && server.user.getGroups().contains(group))
        {
            String message = mPreferenceHelper.getDefaultMessage();
            sendMessageToTopic(
                    group, message, MESSAGE_SOURCE_ALERT
            );
        }

    }


    @SuppressLint("StaticFieldLeak")
    public void sendMessageToTopic(final String topic, final String message, final String source) {
        // Get senderId display name
        final String displayName = getDisplayName();

        // Get the current version of the app
        PackageInfo packageInfo = null;
        int appVersion = -1;
        try {
            packageInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            appVersion = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        final Message.Builder messageBuilder = new Message.Builder();

        // Add data to message
        messageBuilder.addData(Notification.MESSAGE, message);
        messageBuilder.addData(Notification.NAME, displayName);
        messageBuilder.addData(Notification.ID, FirebaseAuth.getInstance().getUid());
        messageBuilder.addData(Notification.TOPIC, topic);
        messageBuilder.addData(Notification.APP_VERSION, String.valueOf(appVersion));
        messageBuilder.addData(Notification.SOURCE, source);

        // Send GCM message in background
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                try {
                    FCMServerSideSender sender = new FCMServerSideSender();

                    // Make sure key exists before sending
                    if (topic != null) {
                        if (server.user.getGroups().contains(topic)) {
                            // Send alert out to topic
                            sender.sendHttpJsonDownstreamMessage("/topics/" + topic, messageBuilder.build());

                            // Log message in FireBase
                            String displayName = getDisplayName();
                            String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();
                            ChatMessage chatMessage = new ChatMessage(topic, userID, displayName, message);
                            logFirebaseMessage(chatMessage);
                        }
                    } else {
                        if (server.user.getGroups().isEmpty()) {
                            return "You have not joined any groups yet";
                        }
                    }
                } catch (IOException e) {
                    return e.getMessage();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                if (result == null) {
                    // Alert has been successfully sent
                    if (!source.equals(Constants.MESSAGE_SOURCE_CHAT)) {
                        Toast.makeText(mContext,
                                "Alert sent successfully to " + topic, Toast.LENGTH_SHORT).show();
                    }

                    // Handle sounds based on preferences
                    boolean notificationsMuted = mPreferenceHelper.getMuted();
                    boolean vibrationEnabled = mPreferenceHelper.getVibrate();

                    if (!notificationsMuted && source.equals(Constants.MESSAGE_SOURCE_ALERT)) {
                        // Play sound if enabled on alert send
                        MediaPlayer mp = MediaPlayer.create(mContext, R.raw.attentionwhistle);
                        mp.start();
                    }
                    if (vibrationEnabled && source.equals(Constants.MESSAGE_SOURCE_ALERT)) {
                        // Vibrate on alert send if enabled
                        Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
                        v.vibrate(500);
                    }
                } else {
                    // Error has occurred
                    Toast.makeText(mContext,
                            "Failed to send message: " + result,
                            Toast.LENGTH_LONG).show();
                }
            }
        }.execute();
    }

    private void logFirebaseMessage(ChatMessage message) {
        // Initiate FireBase reference
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        // Add new messages to messages
        DatabaseReference messageRef = database.getReference(MESSAGES).child(message.getGroup());
        messageRef.child(MESSAGES).push().setValue(message);

        // Set the last message
        Sender sender = Sender.getInstance(mContext);
        String messageString = getDisplayName() + ": " + message.getMessageSent();
        LastMessage lastMessage = new LastMessage(messageString, sender.user.getProfilePicture(), sender.user.getId());

        DatabaseReference groupRef = database.getReference(TOPICS).child(message.getGroup());
        groupRef.child(LAST_MESSAGE).setValue(lastMessage);
    }

}
