package apps.studio.playful.bossalert.models;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Very quick hacky password hashing. Uses SHA-256 with generated salt. May want to look over this
 * because I've only ever done this in PHP and there are built in libraries for this. Majority of
 * the code is pulled from here:
 * <p/>
 * http://stackoverflow.com/questions/18142745/how-do-i-generate-a-salt-in-java-for-salted-hash and
 * http://stackoverflow.com/questions/10129311/does-every-android-phone-support-sha-256
 * <p/>
 * couldn't find much official documentation
 * <p/>
 * TODO: Look over this
 */
public class Password {

    /**
     * Convert raw password with a saved salt to get a hash
     *
     * @param password Raw password entered by the user
     * @param salt     Associated salt
     * @return hashed password
     */
    public static String getHashedPassword(String password, String salt) {
        return bin2Hex(hashPassword(password + salt));
    }

    public static boolean verifyUserPassword(String password, String salt, String storedPassword) {
        String hashedPassword = getHashedPassword(password, salt);
        return hashedPassword.equals(storedPassword);
    }

    /**
     * Generate a binary hashed password based on raw string password
     *
     * @param password  Raw string password
     * @return binary hashed password
     */
    private static byte[] hashPassword(String password) {
        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        digest.reset();
        return digest.digest(password.getBytes());
    }

    /**
     * Converts binary to hexadecimal String value
     *
     * @param data  byte data
     * @return String
     */
    private static String bin2Hex(byte[] data) {
        return String.format("%0" + (data.length * 2) + 'x', new BigInteger(1, data));
    }

    /**
     * Generate a random salt
     *
     * @return Randomly generated salt
     */
    public static String generateSalt() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        return bin2Hex(bytes);
    }
}
