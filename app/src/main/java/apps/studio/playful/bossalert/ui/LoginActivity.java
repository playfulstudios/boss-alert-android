package apps.studio.playful.bossalert.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.helpers.LogUtils;
import apps.studio.playful.bossalert.helpers.PreferenceHelper;
import apps.studio.playful.bossalert.models.Sender;
import apps.studio.playful.bossalert.ui.dialogs.TAndCDialog;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "UI.LoginActivity";

    private static final int RC_SIGN_IN = 1;

    private ProgressDialog mProgressDialog;

    private CallbackManager mCallbackManager;

    // Firebase References
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    // Google References
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);

        // Load terms and conditions if not already accepted
        boolean hasAcceptedTAndC = new PreferenceHelper(this).getBooleanValue(TAndCDialog.KEY, false);
        if (!hasAcceptedTAndC) {
            TAndCDialog dialog = new TAndCDialog(this, this.getLayoutInflater());
            dialog.show();
        }


        // First check if user already exists
        if (checkUserExists())
        {
            initLogin();
        }

        // Load layout
        setContentView(R.layout.activity_login);

        // Initialize the google sign in
        initGoogleSignIn();

        // Initialize the Facebook sign in
        initFacebookSignIn();

        // Setup firebase auth listener
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    LogUtils.Debug(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    if (checkUserExists()) {
                        initLogin();
                    }
                } else {
                    // User is signed out
                    LogUtils.Debug(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        mProgressDialog = new ProgressDialog(this);

    }

    /**
     * Initialize the callback manager for Facebook
     */
    private void initFacebookSignIn()
    {
        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = findViewById(R.id.facebook_login_button);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                firebaseAuthWithFacebook(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                LogUtils.Error(TAG, "facebook:onError\n" + error);
            }
        });
    }

    /**
     * Build the sign in options/button and handle the on click listener
     */
    private void initGoogleSignIn()
    {
        // Initialize Google Login Button
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        findViewById(R.id.google_sign_in_button).setOnClickListener(this);
    }



    /**
     * Authenticate Facebook token with Firebase
     *
     * @param token Facebook Access Token
     */
    private void firebaseAuthWithFacebook(AccessToken token) {
        loadProgressDialog("Signing In", "Signing in with Facebook");

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        mProgressDialog.dismiss();
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            // Double check user is valid and load menu_main activity
                            if (checkUserExists())
                            {
                                initLogin();
                            }

                        }
                    }
                });
    }

    /**
     * Checks that the Google account credentials are validated
     * If so, load the menu_main app content else display error
     *
     * @param acct Google account
     */
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        loadProgressDialog("Signing In", "Signing in with Google");

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        mProgressDialog.dismiss();

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            // Double check user is authenticated nad load activity
                            if (checkUserExists())
                            {
                                initLogin();
                            }
                        }
                    }
                });
    }

    /**
     * Loads the menu_main app activity, clearing the login from the back stack
     */
    private void initLogin()
    {
        // Initialize FireBase reference
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users");

        // Check if user already exists
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                String userId = firebaseUser.getUid();

                // Load sender from Firebase
                Sender sender = Sender.getInstance(getApplication().getApplicationContext());
                sender.loadFromFireBase(userId);

                // Start menu_main activity
                startMainActivity();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    private void startMainActivity() {


        // Create new intent that clears this from back stack
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    /**
     * Helper method for initializing and setting up the progress dialog
     *
     * @param title     Title of the progress dialog
     * @param message   Message of the progress dialog
     */
    private void loadProgressDialog(String title, String message)
    {
        mProgressDialog.setTitle(title);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    /**
     * Checks whether we have user data for the logged in user
     * If not, take the user to the login activity
     */
    private boolean checkUserExists() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        return (user != null);
    }

    /**
     * Fetch results from google sign in activity
     */
    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {

            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.google_sign_in_button:
                googleSignIn();
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

