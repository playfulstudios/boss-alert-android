package apps.studio.playful.bossalert.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

public class Subscription {

    public static final long INFINITE_SUBSCRIPTION = -1;

    @IgnoreExtraProperties
    public enum SUBSCRIPTION_TYPES {
        TEST,
        MONTHLY,
        YEARLY,
        LIFETIME
    }

    private String user;
    private long purchaseTime;
    private SUBSCRIPTION_TYPES type;

    /**
     * Empty for Firebase
     */
    public Subscription() {

    }

    public Subscription(String user, long purchaseTime, SUBSCRIPTION_TYPES type) {
        this.user = user;
        this.purchaseTime = purchaseTime;
        this.type = type;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public long getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(long purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public SUBSCRIPTION_TYPES getType() {
        return type;
    }

    public void setType(SUBSCRIPTION_TYPES type) {
        this.type = type;
    }

    /**
     * Return the duration in seconds depending on subscription type
     * @return subscription duration in seconds
     */
    @Exclude
    public long getSubscriptionDuration() {

        long duration = 0;

        switch (type) {
            case TEST:
                duration = 5000;
                break;
            case MONTHLY:
                duration = 30 * 24 * 60 * 60;
                break;
            case YEARLY:
                duration = 365 * 24 * 60 * 60;
                break;
            case LIFETIME:
                // Use negative one for infinite tim
                duration = -1;
                break;
        }

        return duration;

    }

    /**
     * Returns the end time of the subscription
     * @return  The end time in Unix time
     */
    @Exclude
    public long getSubscriptionEndTime() {
        if (type == SUBSCRIPTION_TYPES.LIFETIME)
        {
            return -1;
        }

        return purchaseTime + getSubscriptionDuration();
    }

    /**
     * Gets the amount of time remaining on the subscription
     * @return The amount of time remaining
     */
    @Exclude
    public long getSubscriptionTimeRemaining() {
        return isSubscriptionValid() ? getSubscriptionEndTime() - System.currentTimeMillis() : 0;
    }

    /**
     * Checks whether or not the subscription time is still valid
     * @return Valid subscription status
     */
    @Exclude
    public boolean isSubscriptionValid() {
        return System.currentTimeMillis() >= getSubscriptionEndTime();
    }
}
