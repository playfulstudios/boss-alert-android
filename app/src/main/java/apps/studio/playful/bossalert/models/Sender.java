package apps.studio.playful.bossalert.models;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import apps.studio.playful.bossalert.helpers.LogUtils;

import static apps.studio.playful.bossalert.helpers.Constants.APP_TOKEN;
import static apps.studio.playful.bossalert.helpers.Constants.DISPLAY_NAME;
import static apps.studio.playful.bossalert.helpers.Constants.EMAIL;
import static apps.studio.playful.bossalert.helpers.Constants.FAVOURITE_GROUP;
import static apps.studio.playful.bossalert.helpers.Constants.ID;
import static apps.studio.playful.bossalert.helpers.Constants.PROFILE_PICTURE;
import static apps.studio.playful.bossalert.helpers.Constants.USERS;
import static apps.studio.playful.bossalert.helpers.Constants.USER_GROUPS;

public class Sender {

    // Constants
    public static final String TAG = "Sender";
    public static final String DEFAULT_SENDER_ID = "763790176833";
    private static final String FILE_NAME = "bossalert_user.json";

    public static volatile Sender mInstance;

    // Class fields
    public User user;
    public String senderId;
    private Context mContext;
    private String appToken;

    /**
     * Create a new sender object without the user profile. Load should be called immediately after
     * Creating this object
     *
     * @param context Application context
     */
    public Sender(Context context){
        mContext = context;
        senderId = DEFAULT_SENDER_ID;
        user = null;
    }

    /**
     * Create a new sender object with the user profile information already loaded
     *
     * @param context Application context
     * @param user    User information
     */
    public Sender(Context context, User user) {
        mContext = context;
        senderId = DEFAULT_SENDER_ID;
        this.user = user;
    }

    public static Sender getInstance(Context context) {
        if (mInstance == null) {
            synchronized (Sender.class) {
                if (mInstance == null) {
                    mInstance = new Sender(context);
                }
            }
        }

        return mInstance;
    }

    public void setUser(User user) {
        this.user = user;
        save();
    }

    /**
     * Reads in a new Sender from a JSON Object
     *
     * @param jsonObject JSONObject containing sender information
     * @throws JSONException
     */
    private void fromJSON(JSONObject jsonObject) throws JSONException {

        // Get the current user objects from the JSON
        String displayName = jsonObject.getString(DISPLAY_NAME);
        String email = "";
        try
        {
            email = jsonObject.getString(EMAIL);
        } catch (JSONException je) {
            LogUtils.Info(TAG, "Failed to find email. This is a temporary fix");
        }

        String id = jsonObject.getString(ID);
        String profilePicture = jsonObject.getString(PROFILE_PICTURE);
        String appToken = jsonObject.getString(APP_TOKEN);
        String favouriteGroup = jsonObject.getString(FAVOURITE_GROUP);

        // Get all the current groups
        JSONArray groupsJArray = jsonObject.getJSONArray(USER_GROUPS);
        List<String> groupsArray = new ArrayList<>();
        if (user != null) {
            groupsArray = user.getGroups();
        }
        if (groupsJArray != null) {
            for (int i = 0; i < groupsJArray.length(); i++) {
                String topic = groupsJArray.get(i).toString();

                if (!groupsArray.contains(topic)) {
                    groupsArray.add(topic);
                }
            }
        }

        // Create user object
        user = new User(id, displayName, profilePicture, email, groupsArray, favouriteGroup);
        this.appToken = appToken;
    }

    /**
     * Convderts the Sender object to a JSONObject
     *
     * @return Sender as JSON Object
     * @throws JSONException
     */
    private JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();

        // Save user to JSON object
        jsonObject.put(ID, user.getId());
        jsonObject.put(DISPLAY_NAME, user.getDisplayName());
        jsonObject.put(PROFILE_PICTURE, user.getProfilePicture());
        jsonObject.put(EMAIL, user.getEmail());
        jsonObject.put(APP_TOKEN, appToken);
        jsonObject.put(FAVOURITE_GROUP, user.getFavouriteGroup());

        // Create JSONArray of groups
        JSONArray groupsJArray = new JSONArray();
        if (user.getGroups() != null) {
            for (int i = 0; i < user.getGroups().size(); i++) {
                groupsJArray.put(user.getGroups().get(i));
            }
        }
        else
        {
            user.setGroups(new ArrayList<String>());
        }
        jsonObject.put(USER_GROUPS, groupsJArray);

        return jsonObject;
    }

    /**
     * Load the object from the file as JSON and convert it back to the object
     *
     * @throws SenderFileException
     */
    public void load() throws SenderFileException {
        File file = new File(mContext.getFilesDir(), FILE_NAME);

        // Make sure file exists
        if (!file.exists() || file.length() == 0) {
            // File does not exist so we need to create a new user
            LogUtils.Info(TAG,"The save file doesn't exist so attempting to load from Firebase");
            loadFromFireBase();
        } else {
            // Default to null for error handling
            FileInputStream inputStream = null;

            // Get meta info about the file
            int fileLength = (int) file.length();
            byte[] fileBytes = new byte[fileLength];

            try {
                // Attempt to read in the JSON from the file
                inputStream = new FileInputStream(file);
                int fileBytesRead = inputStream.read(fileBytes);

                // Failed to read in the file
                if (fileBytesRead == -1) {
                    LogUtils.Error(TAG, "Failed to read sender files");
                } else {
                    // Get JSON object about the user and load it into the object
                    JSONObject senderObject = new JSONObject(new String(fileBytes));
                    fromJSON(senderObject);
                }

            } catch (JSONException | IOException e) {
                e.printStackTrace();
            } finally {
                // Finished with file, closing
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Save the object to the save folder in JSON format
     */
    public void save() {
        // Attempt to write the JSON object to file
        if (user != null) {

            // Load the output file
            File file = new File(mContext.getFilesDir(), FILE_NAME);
            FileOutputStream outputStream = null;

            // Attempt to save the JSON to a file
            try {
                outputStream = new FileOutputStream(file);
                outputStream.write(toJSON().toString().getBytes());
                outputStream.close();
                LogUtils.Info(TAG, "Writing new sender to " + file.getAbsolutePath() + "\n\t" + toJSON().toString());
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }


            saveToFireBase();
        }


    }

    public boolean isEmpty()
    {
        return user == null;
    }

    private void saveToFireBase()
    {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        if (user.getFavouriteGroup() == null)
        {
            if (user.getGroups() != null)
            {
                if (!user.getGroups().isEmpty()) {
                    user.setFavouriteGroup(user.getGroups().get(0));
                }
            }
        }
        databaseReference.child(USERS).child(user.getId()).setValue(user);
    }

    public void loadFromFireBase(final String userId)
    {

        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child(USERS).child(userId);


        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user == null)
                {
                    // User doesn't exist, create user
                    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

                    user = new User();

                    // Build a new user (ignoring all the info loaded from FirebaseDatabase)
                    user.setId(firebaseUser.getUid());
                    user.setDisplayName(firebaseUser.getDisplayName());
                    user.setProfilePicture((firebaseUser.getPhotoUrl().toString()));
                    user.setEmail(firebaseUser.getEmail());
                    user.setGroups(new ArrayList<String>());
                    user.setFavouriteGroup(null);


                    // Send the new user to Firebase
                    ref.setValue(user);
                    setUser(user);
                }
                else
                {
                    // User already exists
                    setUser(user);
                }


                LogUtils.Info(TAG, "Fetched user information from Firebase" );
                save();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });
    }


    public void loadFromFireBase()
    {
        final String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        loadFromFireBase(userId);

    }


    public String getAppToken() {
        return appToken;
    }

    public void setAppToken(String appToken) {
        this.appToken = appToken;
        save();
    }

    /**
     * Custom exception specifically for handling an invalid sender file
     */
    public class SenderFileException extends FileNotFoundException {
        SenderFileException(String message) {
            super(message);
        }
    }
}
