package apps.studio.playful.bossalert.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v7.preference.PreferenceManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.helpers.Constants;
import apps.studio.playful.bossalert.ui.MainActivity;

import static apps.studio.playful.bossalert.helpers.Constants.MESSAGE_SOURCE_ALERT;
import static apps.studio.playful.bossalert.helpers.Constants.MESSAGE_SOURCE_CHAT;
import static apps.studio.playful.bossalert.helpers.Constants.Notification;
import static apps.studio.playful.bossalert.helpers.Constants.Notification.ALERT_CHANNEL_ID;
import static apps.studio.playful.bossalert.helpers.Constants.Notification.ALERT_CHANNEL_NAME;
import static apps.studio.playful.bossalert.helpers.Constants.Notification.ALERT_CHANNEL_DESC;
import static apps.studio.playful.bossalert.helpers.Constants.Notification.CHAT_CHANNEL_DESC;
import static apps.studio.playful.bossalert.helpers.Constants.Notification.CHAT_CHANNEL_ID;
import static apps.studio.playful.bossalert.helpers.Constants.Notification.CHAT_CHANNEL_NAME;


public class FCMService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);

        // Retrieve information from notification
        Map data = remoteMessage.getData();

        // Separate all information
        String displayName = data.get(Notification.NAME).toString();
        String senderID = data.get(Notification.ID).toString();
        String message = data.get(Notification.MESSAGE).toString();
        String group = data.get(Notification.TOPIC).toString();
        String source = MESSAGE_SOURCE_ALERT;

        // New App Information
        int appVersion;
        try {
            appVersion = Integer.parseInt(data.get("appVersion").toString());
        } catch (Exception e) {
            appVersion = -1;
        }

        // Handle version specific information
        if (appVersion >= 3) {
            source = data.get("messageSource").toString();
        }

        // Set title
        String titleToShow = "New alert from " + group;
        if (source.equals(MESSAGE_SOURCE_CHAT)) {
            titleToShow = "New message from " + group;
        }
        String messageToShow = displayName + ": " + message;

        try {
            // If the user sent the message whilst showing their name, don't show notification
            String currentUser = FirebaseAuth.getInstance().getUid();
            if (!senderID.equals(currentUser)) {
                handleNotification(messageToShow, titleToShow, group, source);
            }
        } catch (NullPointerException e) {
            // Firebase issue, show notification anyway
            handleNotification(messageToShow, titleToShow, group, source);
        }
    }

    private void handleNotification(String message, String title, String group, String source) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if (!group.equals("")) {
            notificationIntent.putExtra("chatFragment", group);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request Code */,
                notificationIntent, PendingIntent.FLAG_ONE_SHOT);

        // Check user preferences for notification settings
        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean mutedNotfications = mSharedPreferences.getBoolean("key_mute_alerts", false);
        boolean useSystemSound = mSharedPreferences.getBoolean("key_system_notification", false);
        boolean vibrate = mSharedPreferences.getBoolean("key_vibrate", true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // The channel ID is defaulted to alert for all devices < build 27
        String channel_id = ALERT_CHANNEL_ID;

        // Because we are targeting Android > 27, we need to ensure that we have notification channels setup
        // otherwise we don't receive alerts :(
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // Set the default notification channel settings to be that of alert
            int importance = NotificationManager.IMPORTANCE_HIGH;

            CharSequence channel_name = ALERT_CHANNEL_NAME;
            String channel_desc = ALERT_CHANNEL_DESC;

            if (source.equals(MESSAGE_SOURCE_ALERT))
            {
                // Change the settings to match the chat notifications and lower the importance to
                // not abuse notifications
                channel_id = CHAT_CHANNEL_ID;
                channel_name = CHAT_CHANNEL_NAME;
                channel_desc = CHAT_CHANNEL_DESC;
                importance = NotificationManager.IMPORTANCE_DEFAULT;
            }

            // Build the notification channel
            NotificationChannel channel = new NotificationChannel(channel_id, channel_name, importance);
            channel.setDescription(channel_desc);
            channel.enableLights(true);
            channel.setLightColor(Color.CYAN);
            channel.enableVibration(true);
            notificationManager.createNotificationChannel(channel);


        }


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channel_id)
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_ba_notification_icon)
                .setContentIntent(pendingIntent);


        if (vibrate) {
            notificationBuilder.setVibrate(new long[]{100, 500, 200});
        }

        // Play sound on notification relieve if not muted
        if (!mutedNotfications) {
            Uri notificationSound;
            if (useSystemSound) {
                // Get the default notification sound
                notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            } else {
                // Use sound depending on source
                int sound = source.equals(MESSAGE_SOURCE_ALERT) ? R.raw.diddlesbark : R.raw.attentionwhistle;
                notificationSound = Uri.parse("android.resource://" + this.getPackageName() + "/" + sound);
            }
            notificationBuilder.setSound(notificationSound);
        }

        notificationManager.notify(title, 0, notificationBuilder.build());

    }

    @Override
    public void onDeletedMessages() {
       super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String msgId) {
        //handleNotification("Upstream message sent. id = " + msgId, "Boss Alert", -1, Constants.MESSAGE_SOURCE_ALERT);
    }

    @Override
    public void onSendError(String msgId, Exception error) {
        //handleNotification("Upstream message send error. id = " + msgId + ", error = " + error, "Boss Alert", -1, Constants.MESSAGE_SOURCE_ALERT);
    }
}
