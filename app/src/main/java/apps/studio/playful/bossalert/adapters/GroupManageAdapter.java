package apps.studio.playful.bossalert.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.models.ChatUser;
import apps.studio.playful.bossalert.models.Sender;
import apps.studio.playful.bossalert.models.User;
import apps.studio.playful.bossalert.ui.fragments.GroupManagementFragment;
import apps.studio.playful.bossalert.ui.views.RoundedImageView;

import static android.graphics.Color.BLACK;

public class GroupManageAdapter extends ArrayAdapter<ChatUser> {

    //
    // List View Members
    //
    private List<ChatUser> mUsers;
    private Map<String, Boolean> mBanList;
    //
    // Other
    //
    private Context mContext;

    public GroupManageAdapter(Context context, int resource, List<ChatUser> users)
    {
        super(context, resource);

        mUsers = users;
        mContext = context;
    }

    public void refresh(List<ChatUser> users, Map<String, Boolean> banList)
    {

        mUsers = users;
        mBanList = banList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        UserViewHolder viewHolder;

        // Check if viewholder already exists
        if (convertView == null) {
            // Doesn't exist, inflate it
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_member, parent, false);

            // Init viewholder
            viewHolder = new UserViewHolder();
            viewHolder.vhProfilePicture = convertView
                    .findViewById(R.id.riv_manage_user_pic);
            viewHolder.vhUserName = convertView.findViewById(R.id.tv_manage_user_name);
            viewHolder.vhUserStatus = convertView.findViewById(R.id.tv_manage_user_status);

            convertView.setTag(viewHolder);
        } else {
            // viewholder exists so load existing view
            viewHolder = (UserViewHolder) convertView.getTag();
        }

        // Get the current user information and load into view holder
        ChatUser user = getItem(position);
        if (user != null) {
            boolean isBanned = mBanList.get(user.getId());
            viewHolder.vhProfilePicture.setImageDrawable(ContextCompat.getDrawable(mContext,
                    R.drawable.com_facebook_profile_picture_blank_portrait));

            // Load the profile picture with Picasso
            Picasso.with(mContext)
                    .load(user.getProfilePicture())
                    .error(R.drawable.com_facebook_profile_picture_blank_portrait)
                    .into(viewHolder.vhProfilePicture);

            // Set the text views
            viewHolder.vhUserName.setText(user.getDisplayName());

            // TODO: Add implementation to fetch banned status of user (wip)
            if (isBanned) {
                viewHolder.vhUserStatus.setText("Banned");
            } else {
                viewHolder.vhUserStatus.setText("");
            }

            // For now, there is only one admin and they are the ones with access to the group
            // TODO: Add validation for entering group management
            if (user.getId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                viewHolder.vhUserStatus.setText("Admin");
                viewHolder.vhUserStatus.setTextColor(BLACK);
            }

        }

        return convertView;

    }

    @Override
    public int getCount() {
        return mUsers.size();
    }

    @Nullable
    @Override
    public ChatUser getItem(int position) {
        return mUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class UserViewHolder
    {
        RoundedImageView vhProfilePicture;
        TextView vhUserName;
        TextView vhUserStatus;
    }
}
