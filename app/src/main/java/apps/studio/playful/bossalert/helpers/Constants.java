package apps.studio.playful.bossalert.helpers;

/**
 * Constants used for persisting the app's objects
 */
public class Constants
{

    public static class Notification {
        public static final String MESSAGE = "message";
        public static final String NAME = "senderName";
        public static final String ID = "senderID";
        public static final String TOPIC = "topic";
        public static final String APP_VERSION = "appVersion";
        public static final String SOURCE = "messageSource";
        public static final String ALERT_CHANNEL_ID = "alert";
        public static final String ALERT_CHANNEL_NAME = "Boss Alerts";
        public static final String ALERT_CHANNEL_DESC = "Important Boss Alerts";
        public static final String CHAT_CHANNEL_ID = "bossChat";
        public static final String CHAT_CHANNEL_NAME = "Messages";
        public static final String CHAT_CHANNEL_DESC = "Chat notifications for Boss Alert";
    }

    public static class FirebaseKeys {
        public static final String TOPICS = "topics";
        public static final String GROUPS = "groups";
        public static final String LAST_MESSAGE = "lastMessage";
        public static final String NICKNAME = "nickName";
    }

    // Sender constants
    public static class Sender {
        public static final String NAME = "name";
        public static final String SENDER_ID = "senderId";
        public static final String API_KEYS = "apiKeys";
        public static final String TEST_APP_TOKEN = "appToken";
        public static final String OTHER_TOKENS = "otherTokens";
    }


    // DeviceGroup constants
    public static final String NOTIFICATION_KEY_NAME = "notificationKeyName";
    public static final String NOTIFICATION_KEY = "notificationKey";
    public static final String TOKENS = "tokens";
    // TaskTracker
    public static final String TAG = "TAG";
    public static final String WINDOW_START_ELAPSED_SECS = "WINDOW_START_ELAPSED_SECS";
    public static final String WINDOW_STOP_ELAPSED_SECS = "WINDOW_STOP_ELAPSED_SECS";
    public static final String PERIOD = "PERIOD";
    public static final String FLEX = "FLEX";
    public static final String CREATED_AT_ELAPSED_SECS = "CREATED_AT_ELAPSED_SECS";
    public static final String CANCELLED = "CANCELLED";
    public static final String EXECUTED = "EXECUTED";

    public static final String MESSAGE_SOURCE_ALERT = "alert";
    public static final String MESSAGE_SOURCE_CHAT = "chat";

    // UI Constants
    public static class UI_TEXT {
        public static final String CANCEL = "cancel";
        public static final String JOIN = "join";
        public static final String CONNECTED = "Connected";
        public static final String DISCONNECTED = "Disconnected";
    }


    // FireBase constants
    public static final String MESSAGES = "messages";
    public static final String USERS = "users";
    public static final String DISPLAY_NAME = "displayName";
    public static final String EMAIL = "email";
    public static final String ID = "id";
    public static final String PROFILE_PICTURE = "profilePicture";
    public static final String LAST_MESSAGE = "lastMessage";
    public static final String PASSWORD = "password";
    public static final String SALT = "salt";
    public static final String USER_GROUPS = "userGroups";
    public static final String APP_TOKEN = "appToken";
    public static final String FAVOURITE_GROUP = "favouriteGroup";
    public static final String LOCKED = "locked";
    public static final String OWNER = "owner";
    public static final String SUBSCRIPTION = "subscription";
    public static final String SUBSCRIPTIONS = "subscriptions";
    public static final String SUBSCRIPTION_EXPIRY = "subscription_expiry";

    public static final String ANONYMOUS = "Anonymous";

    // Color constants
    public static final String ORANGE = "orange";
    public static final String BLUE = "blue";

    private Constants() {

    }
}