package apps.studio.playful.bossalert.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;

public class PreferenceHelper {

    private SharedPreferences mSharedPreferences;

    /**
     * Helps contain all the preferences that will be used throughout the program. Simply to make
     * things a little bit tidier and have a way to access all the preferences without having to
     * type in the keys
     */
    public PreferenceHelper(Context context) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Returns any boolean value within preferences
     *
     * @param key          The key to search for
     * @param deafultValue The default value for the preference if not set
     * @return Preference value
     */
    public boolean getBooleanValue(String key, boolean deafultValue) {
        return mSharedPreferences.getBoolean(key, deafultValue);
    }

    public long getLongValue(String key, long defaultValue) {
        return mSharedPreferences.getLong(key, defaultValue);
    }

    /**
     * Returns any string value within preferences
     *
     * @param key          The key to search for
     * @param defaultValue The default value for the preference if not set
     * @return Preference value
     */
    public String getStringValue(String key, String defaultValue) {
        return mSharedPreferences.getString(key, defaultValue);
    }

    /**
     * @return Whether the users full name is displayed
     */
    public boolean getNameIsDisplayed() {
        String key = "key_show_name";
        return mSharedPreferences.getBoolean(key, true);
    }

    /**
     * @return Whether system notification sound will be used
     */
    public boolean getUsingNotificationSound() {
        String key = "key_system_notification";
        return mSharedPreferences.getBoolean(key, false);
    }

    /**
     * @return Whether the phone will vibrate on various actions
     */
    public boolean getVibrate() {
        String key = "key_vibreate";
        return mSharedPreferences.getBoolean(key, true);
    }

    /**
     * @return Whether the notifications are muted
     */
    public boolean getMuted() {
        String key = "key_mute_alerts";
        return mSharedPreferences.getBoolean(key, false);
    }

    /**
     * @return The default message to be sent when sending a quick alert
     */
    public String getDefaultMessage() {
        String key = "key_default_message";
        String defaultValue = "has sent an alert";
        return mSharedPreferences.getString(key, defaultValue);
    }
}
