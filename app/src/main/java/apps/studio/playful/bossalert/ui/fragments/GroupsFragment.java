package apps.studio.playful.bossalert.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.helpers.PreferenceHelper;
import apps.studio.playful.bossalert.logic.NotificationSender;
import apps.studio.playful.bossalert.models.ChatMessage;
import apps.studio.playful.bossalert.models.Group;
import apps.studio.playful.bossalert.models.LastMessage;
import apps.studio.playful.bossalert.models.Sender;
import apps.studio.playful.bossalert.ui.MainActivity;
import apps.studio.playful.bossalert.ui.dialogs.GroupMessageSendDialog;
import apps.studio.playful.bossalert.ui.views.RoundedImageView;

import static apps.studio.playful.bossalert.helpers.Constants.ANONYMOUS;
import static apps.studio.playful.bossalert.helpers.Constants.MESSAGE_SOURCE_ALERT;
import static apps.studio.playful.bossalert.helpers.Constants.FirebaseKeys.TOPICS;


public class GroupsFragment extends Fragment {

    public static final String TAG = "GroupsFragment";

    //
    // Layout Members
    //
    private View mView;
    private LayoutInflater mLayoutInflater;
    private RecyclerView mGroupsRecyclerView;
    private AdapterView.AdapterContextMenuInfo mMenuInfo;


    //
    // Other
    //
    private OnGroupSelectedListener mCallback;
    private Context mContext;
    private PreferenceHelper mPreferenceHelper;
    private NotificationSender mNotificationSender;
    private Sender mSender;
    private List<Group> mGroupList;
    private GroupsRecyclerAdapter mAdapter;
    private boolean mGroupsLoaded = false;
    private ProgressBar mLoadingBar;

    public static GroupsFragment newInstance()
    {
        return new GroupsFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this.getActivity().getWindow().getContext();
        mPreferenceHelper = new PreferenceHelper(mContext);
        mNotificationSender = new NotificationSender(mContext);
        mSender = Sender.getInstance(mContext);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mLayoutInflater = inflater;
        mView = mLayoutInflater.inflate(R.layout.fragment_groups, container, false);

        // Show the floating action button and change title
        ((MainActivity) getActivity()).showFloatingActionButton();
        ((MainActivity) getActivity()).setToolbarTitle(getString(R.string.groups));

        // Setup the layout fields
        mGroupsRecyclerView = mView.findViewById(R.id.rv_groups);
        TextView mTextNoGroup = mView.findViewById(R.id.tv_no_groups_joined);
        TextView mPlsJoinGroup = mView.findViewById(R.id.tv_join_group_helper);

        // Load the progress bar
        mLoadingBar = mView.findViewById(R.id.pb_loading_groups);
        if (!mGroupsLoaded)
            mLoadingBar.setVisibility(View.VISIBLE);

        // Load Data
        mSender = Sender.getInstance(mContext);
        initRecyclerView();

        if(mSender.user.getGroups().size() == 0){ //isEmpty()-throws nullPointerException if the list is null
            mLoadingBar.setVisibility(View.GONE);
            mTextNoGroup.setVisibility(View.VISIBLE);
            mPlsJoinGroup.setVisibility(View.VISIBLE);

        }
        else {
            mTextNoGroup.setVisibility(View.GONE);
            mPlsJoinGroup.setVisibility(View.GONE);
        }

        // Remove all notifications from Boss Alert
        NotificationManager notifManager= (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notifManager != null) {
            notifManager.cancelAll();
        }

        return mView;
    }

    /**
     * Share the group with a link that will open the join fragment
     * @param group The group to share
     */
    protected void shareGroup(Group group) {

        // Create the share intent
        Intent shareTopicIntent = new Intent(Intent.ACTION_SEND);
        shareTopicIntent.setType("text/plain");

        // Add info to share topic
        shareTopicIntent.putExtra(Intent.EXTRA_SUBJECT, "Join my group on Boss Alert");
        shareTopicIntent.putExtra(Intent.EXTRA_TITLE, "Join my group on Boss Alert!");
        shareTopicIntent.putExtra(Intent.EXTRA_TEXT, "Here's an invite to join \"" + group.getName() +
                "\" on Boss Alert: http://www.playfulstudios.com.au/bossalert/join/" + group.getName());
        startActivity(Intent.createChooser(shareTopicIntent, "Share via..."));
    }

    /**
     * Sends the users default message to the group
     * @param group The group to send the message to
     */
    protected void sendAlert(Group group) {
        String alertMessage = mPreferenceHelper.getDefaultMessage();

        // Send alert
        mNotificationSender.sendMessageToTopic(
                group.getName(),
                alertMessage,
                MESSAGE_SOURCE_ALERT
        );
    }

    /**
     * Deletes the group from the users group list
     * @param group Group to delete
     */
    protected void deleteGroup(Group group)
    {
        // Remove groups from list
        mSender.user.getGroups().remove(group.getName());
        mGroupList.remove(group);

        // Ensure it wasn't the users favourite group
        if (mSender.user.getFavouriteGroup().equals(group.getName()))
        {
            mSender.user.setFavouriteGroup(null);
        }

        // Unsubscribe from Firebase
        FirebaseMessaging.getInstance().unsubscribeFromTopic(group.getName());

        // Save changes
        mSender.save();

        // Update listview
        mAdapter.notifyDataSetChanged();
    }


    /**
     * Send a message (from dialog) to the group
     * @param group The group to send the message to
     */
    protected void sendMessageViaDialog(Group group)
    {
        GroupMessageSendDialog messageSendDialog = new GroupMessageSendDialog(mContext,
                group.getName(), mNotificationSender, mLayoutInflater);
        messageSendDialog.show();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (OnGroupSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnGroupSelectedListener");
        }
    }


    /**
     * Starts a group listener based on the group name and performs some housekeeping to ensure
     * everything is valid
     *
     * @param ref   Firebase reference (prevents creating one over and over again)
     * @param group The group name to start the listener
     */
    private void startGroupListener(DatabaseReference ref, final String group) {
        // Don't handle if group is null
        if (group == null)
            return;

        ref.child(group).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Made connection so remove progress bar
                mGroupsLoaded = true;
                mLoadingBar.setVisibility(View.GONE);

                // Extract group info from the data snapshot
                Group groupInfo = dataSnapshot.getValue(Group.class);
                if (groupInfo == null) return;

                // The key to the group is the name of the group
                String groupName = dataSnapshot.getKey();
                groupInfo.setName(groupName);

                // Check if group exists already in the list
                if (mGroupList.contains(groupInfo)) {
                    // Just refresh the individual list view element
                    int index = mGroupList.indexOf(groupInfo);
                    mAdapter.notifyItemChanged(index);
                } else {
                    // Check if the group is locked
                    if (groupInfo.isLocked()) {

                        // Delete the group from the users list
                        mSender.user.removeGroup(groupInfo.getName());

                        // Group has been deleted since last sync with server
                        // Remove from favourites if required
                        if (groupInfo.getName().equals(mSender.user.getFavouriteGroup())) {
                            mSender.user.setFavouriteGroup(null);
                        }
                    } else {
                        // User has no favourite group so just add the first one that comes in
                        if (mSender.user.getFavouriteGroup() == null) {
                            mSender.user.setFavouriteGroup(groupInfo.getName());
                        }

                        // Subscribe via Firebase messaging
                        FirebaseMessaging.getInstance().subscribeToTopic(groupInfo.getName());

                        // Update the ListView
                        mGroupList.add(groupInfo);
                        mAdapter.notifyItemInserted(mGroupList.size() - 1);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void initRecyclerView() {

        // Initialize the recycler view
        if (mGroupList == null) {
            // Create empty group list and attempt to load data
            mGroupList = new ArrayList<>();
            mSender.user.initGroups();
        }

        // Create Firebase reference
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference().child(TOPICS);

        // Create listeners for each group
        for (final String group : mSender.user.getGroups()) {
            startGroupListener(ref, group);
        }

        // Init the adapter
        mAdapter = new GroupsRecyclerAdapter(mContext, mGroupList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        mGroupsRecyclerView.setLayoutManager(layoutManager);
        mGroupsRecyclerView.setAdapter(mAdapter);
    }

    public interface OnGroupSelectedListener
    {
        void onGroupSelected(String group);
    }

    public class GroupsRecyclerAdapter extends RecyclerView.Adapter<GroupsRecyclerAdapter.ViewHolder> {

        public static final String TAG = "GroupsRecyclerAdapter";

        private List<Group> mGroups;
        final protected Context mContext;

        public GroupsRecyclerAdapter(Context mContext, List<Group> mGroups) {
            this.mGroups = mGroups;
            this.mContext = mContext;
        }

        @Override
        public GroupsRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            // Load layout
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_groups, parent, false);

            // Create the viewholder
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            // Get all data
            final Group group = mGroups.get(position);
            if (group == null) return; // No group so fail

            // Load the data into the viewholder
            holder.groupNameTV.setText(group.getNickName());

            // Load last message details if it exists
            LastMessage lastMessage = group.getLastMessage();
            updateLastMessage(holder, lastMessage, position);

            // Set listener for changing the data
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = database.getReference().child(TOPICS).child(group.getName());
            ChildEventListener listener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    try {
                        LastMessage lastMessage = dataSnapshot.getValue(LastMessage.class);
                        if (lastMessage != null) {
                            mGroups.get(position).setLastMessage(lastMessage);
                            updateLastMessage(holder, lastMessage, position);
                            notifyItemChanged(position);
                        }
                    } catch (DatabaseException dbe) {
                        try {
                            String nickName = dataSnapshot.getValue(String.class);
                            mGroups.get(position).setNickName(nickName);
                            holder.groupNameTV.setText(nickName);
                            notifyItemChanged(position);
                        } catch (DatabaseException dbe2) {

                        }
                    }



                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            };

            ref.addChildEventListener(listener);

            // Set on click listener for view holder
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.onGroupSelected(mGroupList.get(holder.getAdapterPosition()).getName());
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final CharSequence[] menuItems = {
                            mContext.getString(R.string.topic_send_alert),
                            mContext.getString(R.string.topic_send_message),
                            mContext.getString(R.string.topic_share),
                            mContext.getString(R.string.topic_delete)
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Select Action");
                    builder.setItems(menuItems, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int position) {
                            switch (position) {
                                case 0:
                                    // Send alert
                                    sendAlert(group);
                                    break;
                                case 1:
                                    // Send Message
                                    sendMessageViaDialog(group);
                                    break;
                                case 2:
                                    // Share group
                                    shareGroup(group);
                                    break;
                                case 3:
                                    // Delete group
                                    deleteGroup(group);
                            }
                        }
                    });
                    builder.show();
                    return true;
                }
            });
        }

        private void updateLastMessage(ViewHolder holder, LastMessage lastMessage, int position) {

            String lastMessageText = mContext.getString(R.string.new_group);
            String userPicture = null;
            String timeStampText = "No Date Found";

            // Don't update if last message is null
            if (lastMessage != null) {

                if (lastMessage.getLastMessage() != null) {
                    lastMessageText = lastMessage.getLastMessage();

                    // Safety for older versions that don't have timestamp on group
                    if(lastMessage.getTimeStamp() != null) {
                        timeStampText = ChatMessage.getUserFriendlyTime((long) lastMessage.getTimeStamp()
                                .get("timestamp"));
                    }
                    else {
                        timeStampText = "";
                    }

                    if (!lastMessage.getLastMessage().split(":")[0].equals(ANONYMOUS)) {
                        userPicture = lastMessage.getUserPicture();
                    }
                }



            }

            // Set the last message TV to either new group or last message
            // Load the profile image
            if (userPicture == null) {
                Picasso.with(mContext)
                        .load(R.drawable.com_facebook_profile_picture_blank_portrait)
                        .into(holder.profilePictureIV);
            } else {
                Picasso.with(mContext)
                        .load(userPicture)
                        .error(R.drawable.com_facebook_profile_picture_blank_portrait)
                        .into(holder.profilePictureIV);
            }

            holder.lastMessageTV.setText(lastMessageText);
            holder.timeStampTV.setText(timeStampText);
        }

        @Override
        public int getItemCount() {
            return mGroups.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            RoundedImageView profilePictureIV;
            TextView groupNameTV;
            TextView lastMessageTV;
            TextView timeStampTV;

            private ContextMenu.ContextMenuInfo mMenuInfo;

            public ViewHolder(View itemView) {
                super(itemView);

                // Load all the layout elements
                profilePictureIV = itemView.findViewById(R.id.riv_group_profile_pic);
                groupNameTV = itemView.findViewById(R.id.tv_group_list_name);
                lastMessageTV = itemView.findViewById(R.id.tv_group_list_last_msg);
                timeStampTV = itemView.findViewById((R.id.tv_group_list_timestamp));

            }


        }
    }
}
