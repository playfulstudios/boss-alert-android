package apps.studio.playful.bossalert.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;

import apps.studio.playful.bossalert.R;
import apps.studio.playful.bossalert.helpers.PreferenceHelper;

/**
 * Created by brand on 13/08/2017.
 */

public class TAndCDialog  extends AlertDialog {


    private static final String TITLE = "Terms and Conditions";
    public static final String KEY = "agree_t_and_c";

    private Context mContext;
    private SharedPreferences mPreferences;


    public TAndCDialog(Context context, LayoutInflater inflater) {
        super(context);

        mContext = context;
        mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        View TAndCView = inflater.inflate(R.layout.dialog_tandc, null);

        setTitle(TITLE);
        setView(TAndCView);
        setButton(BUTTON_POSITIVE, "Agree", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor prefManager =  mPreferences.edit();
                prefManager.putBoolean(KEY, true);
                prefManager.apply();

            }
        });

        setButton(BUTTON_NEGATIVE, "Disagree", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor prefManager =  mPreferences.edit();
                prefManager.putBoolean(KEY, false);
                prefManager.apply();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

    }




}
